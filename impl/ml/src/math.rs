pub fn sigmoid(z: f64) -> f64 {
    1.0 / (1.0 + (-z).exp())
}

/// Sigmoid derived with respect to weights.
pub fn sigmoid_derived(z: f64) -> f64 {
    sigmoid(z) * (1.0 - sigmoid(z))
}
