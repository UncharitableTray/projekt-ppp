extern crate linalg;

use linalg::vector::RealVec;

pub trait Labeled<L> {
    fn feats(&self) -> &RealVec;

    fn label(&self) -> &L;
}

pub mod numeric {
    use super::*;

    #[derive(Debug, PartialEq)]
    /// Represents a simple numeric binary class.BinClass
    pub enum BinClass {
        One = 1,
        Zero = 0,
    }

    #[derive(Debug)]
    /// Represents a simple numeric labeled example with a 2D feature vector and a binary label.
    pub struct Numeric {
        feats: RealVec,

        label: BinClass,
    }

    impl Numeric {
        pub fn new(v: RealVec, l: BinClass) -> Numeric {
            Numeric { feats: v, label: l }
        }

        pub fn feats(&self) -> &RealVec {
            &self.feats
        }

        pub fn feats_mut(&mut self) -> &mut RealVec {
            &mut self.feats
        }

        pub fn label(&self) -> &BinClass {
            &self.label
        }

        pub fn set_label(&mut self, new: BinClass) {
            self.label = new
        }
    }

}
