extern crate linalg;
extern crate plotlib;
extern crate rand;

use crate::labeled::numeric::{BinClass, Numeric};
use crate::math::sigmoid;
use linalg::error::MathError;
#[allow(unused_imports)]
use linalg::matrix::{Matrix, RealMat};
#[allow(unused_imports)]
use linalg::vector::{RealVec, Vector};
#[allow(unused_imports)]
use crate::classif::rand::prelude::*;
//use crate::rand::

type Result<T> = std::result::Result<T, MathError>;

// Imports for rng
#[allow(unused_imports)]
use rand::prelude::*;
#[allow(unused_imports)]
use rand::seq::SliceRandom;
use rand::thread_rng;

// Imports for image plotting
use plotlib::page::Page;
use plotlib::scatter::{Scatter, Style};
#[allow(unused_imports)]
use plotlib::style::Point;
use plotlib::view::ContinuousView;

#[derive(Debug)]
pub struct Example2D(usize, Numeric);

/// Provides a simple linear regression model.
pub mod logreg {
    use super::*;

    static THRESHOLD: f64 = 0.5;
    static LEARNING_RATE: f64 = 0.1;
    static EPOCHS: usize = 15000;
    static BATCH_SIZE: usize = 30;

    /// Linear Regression with 2D feature vectors and a binary output
    pub struct LogReg2D {
        examples: Vec<Example2D>,
        weights: RealVec,
    }

    impl LogReg2D {
        pub fn from_inputs(ins: Vec<Example2D>) -> LogReg2D {
            let len = ins[0].1.feats().get_dim();
            let mut ins = ins;
            ins.iter_mut()
                .for_each(|x| x.1.feats_mut().values_mut().push(1.0));
            LogReg2D {
                examples: ins,
                weights: RealVec::from_vec(vec![0.0; len + 1]),
            }
        }

        pub fn from_weights_simulation(w: RealVec, seed: Option<&Vec<u8>>) -> LogReg2D {
            let mut ins = gen_inputs_circle(200, seed);
            ins.iter_mut()
                .for_each(|x| x.1.feats_mut().values_mut().push(1.0));
            LogReg2D {
                examples: ins,
                weights: w,
            }
        }

        pub fn examples_len(&self) -> usize {
            self.examples.len()
        }

        fn classify(&self, f: f64) -> BinClass {
            if f - THRESHOLD >= 0.0 {
                BinClass::One
            } else {
                BinClass::Zero
            }
        }

        fn predict_one(&self, x: &RealVec) -> f64 {
            sigmoid(self.weights.dot(x))
        }

        fn cost_batch(&mut self, size: usize) -> f64 {
            let dim = self.weights.get_dim();
            self.examples.shuffle(&mut thread_rng());
            let sum = self.examples.iter().take(size).fold(0.0, |acc, x| {
                let example = &x.1;
                let class = example.label();
                match class {
                    BinClass::One => acc + self.predict_one(&example.feats()).ln(),
                    BinClass::Zero => acc + (1.0 - self.predict_one(&example.feats())).ln(),
                }
            });
            -1.0 * sum / dim as f64
        }

        fn construct_batch(&self, size: usize) -> (RealMat, RealMat) {
            let xs = self
                .examples
                .iter()
                .take(size)
                .map(|x| x.1.feats().values().clone())
                .collect::<Vec<Vec<f64>>>();

            let ys = self
                .examples
                .iter()
                .take(size)
                .map(|x| match x.1.label() {
                    BinClass::Zero => 0.0,
                    BinClass::One => 1.0,
                })
                .collect::<Vec<f64>>();

            let ys = ys
                .iter()
                .map(|x| {
                    let mut nvec = Vec::new();
                    nvec.push(*x);
                    nvec
                })
                .collect::<Vec<Vec<f64>>>();

            (
                RealMat::from_vec(xs).unwrap(),
                RealMat::from_vec(ys).unwrap(),
            )
        }

        fn predict_batch(&self, size: usize) -> RealVec {
            let mut predictions = Vec::new();
            for i in 0..size {
                let p = self.predict_one(self.examples.get(i).unwrap().1.feats());
                predictions.push(p);
            }
            RealVec::from_vec(predictions)
        }

        pub fn weights(&self) -> &RealVec {
            &self.weights
        }

        pub fn calculate_gradient_batch(&self, size: usize) -> Result<RealVec> {
            let (xs, ys) = self.construct_batch(size);
            let predictions = self.predict_batch(size);

            let s = RealMat::from_vec_into_col(&predictions)
                .sub(&ys)
                .expect("Sub error");
            let grad = xs
                .transpose()
                .expect("Transpose error")
                .mul(&s)
                .expect("Mul error");
            let grad_vec = RealVec::from_col_matrix(&grad).expect("From row matrix error");
            //grad_vec.scale_inplace(LEARNING_RATE / dim as f64);
            Ok(grad_vec)
        }

        fn update_weights_batch(&mut self, size: usize) -> Result<()> {
            let dim = self.weights.get_dim();
            let mut grad_vec = self.calculate_gradient_batch(size)?;
            println!("Gradient vector: {}", grad_vec);
            grad_vec.scale_inplace(LEARNING_RATE / dim as f64);
            println!("Gradient vector after scaling: {}", grad_vec);

            self.weights
                .sub_inplace(&grad_vec)
                .expect("Sub inplace error");
            println!("New weights: {}", self.weights);
            Ok(())
        }

        /// Returns the weight vector after e epochs using bsize batch sizes.
        /// Also returns the value of the cost functions after e such epochs.
        pub fn grad_descent_batch_custom(&mut self, bsize: usize, epochs: usize) -> (RealVec, f64) {
            let mut final_cost = 0.0;
            for _ in 0..epochs {
                //self.examples.shuffle(&mut thread_rng());
                self.update_weights_batch(bsize).expect("Math error.");
                final_cost = self.cost_batch(bsize);
            }
            let weights = self.weights.clone();
            (weights, final_cost)
        }

        pub fn grad_descent_batch(&mut self) {
            let batch_size = BATCH_SIZE;
            let mut cost_history = Vec::with_capacity(EPOCHS);
            for i in 1..=EPOCHS {
                self.examples.shuffle(&mut thread_rng());
                self.update_weights_batch(batch_size).expect("Math error");
                let cost = self.cost_batch(batch_size);

                if i % 500 == 0 {
                    println!("Weights in iter {} : {}", i, self.weights);
                    println!("Cost in iter {}: {}", i, cost);
                }
                cost_history.push(cost);
            }
        }

        pub fn plot_dataset(&self) {
            let data_a = self.feats_by_class(BinClass::One);
            let s1 = Scatter::from_slice(&data_a).style(
                Style::new()
                    .marker(plotlib::style::Marker::Circle)
                    .colour("#DD3355"),
            );
            let data_b = self.feats_by_class(BinClass::Zero);
            let s2 = Scatter::from_slice(&data_b).style(
                Style::new()
                    .marker(plotlib::style::Marker::Cross)
                    .colour("#35C788"),
            );
            let v = ContinuousView::new()
                .add(&s1)
                .add(&s2)
                .x_range(0.0, 15.0)
                .y_range(0.0, 15.0)
                .x_label("X")
                .y_label("Y");
            Page::single(&v).dimensions(431, 431).save("gfx/plot.svg").unwrap();
        }

        pub fn validate(&self) -> f64 {
            let test_examples = gen_inputs_circle(100, None);
            let acc_sum = test_examples
                .iter()
                .map(|x| (x.1.feats(), x.1.label()))
                .fold(0.0, |acc, (fs, lab)| {
                    let pred = self.predict_one(&fs);
                    match self.classify(pred) == *lab {
                        true => acc + 1.0,
                        _ => acc + 0.0,
                    }
                });
            acc_sum / (test_examples.len() as f64)
        }

        fn feats_by_class(&self, class: BinClass) -> Vec<(f64, f64)> {
            self.examples
                .iter()
                .filter(|x| *x.1.label() == class)
                .map(|x| {
                    let feats = x.1.feats().values();
                    (feats[0], feats[1])
                })
                .collect::<Vec<(f64, f64)>>()
        }

        pub fn update_examples(&mut self, n: usize, seed: Option<&Vec<u8>>) {
            self.examples = gen_inputs_circle(n, seed);
        }
    }

    pub fn gen_inputs_circle(n: usize, seed: Option<&Vec<u8>>) -> Vec<Example2D> {
        let c_1_x = 4.0;
        let c_1_y = 10.0;
        let c_2_x = 10.0;
        let c_2_y = 4.0;
        let rad = 3.0;
        let pi = std::f64::consts::PI;
        let mut dataset = Vec::new();

        let mut rng = match seed {
            Some(s) => {
                let mut local_seed = [0u8; 32];
                for (local, outer) in local_seed.iter_mut().zip(s.iter()) {
                    *local = *outer;
                }
                StdRng::from_seed(local_seed)
            },
            None => StdRng::from_rng(thread_rng()).unwrap(),
        };

        for i in 0..n {
            let r1: f64 = rng.gen_range(0.0, 1.0);
            let r2: f64 = rng.gen_range(0.0, 1.0);
            let phi: f64 = 2.0 * pi * r1;
            let r: f64 = rad * r2.sqrt();

            let x;
            let y;
            let c;
            if i % 2 == 0 {
                x = r * (phi.cos()) + c_1_x;
                y = r * (phi.sin()) + c_1_y;
                c = BinClass::One;
            } else {
                x = r * (phi.cos()) + c_2_x;
                y = r * (phi.sin()) + c_2_y;
                c = BinClass::Zero;
            }
            let v = RealVec::from_vec(vec![x, y]);
            let d = Numeric::new(v, c);
            dataset.push(Example2D(i, d));
        }
        dataset.shuffle(&mut thread_rng());
        dataset
    }
}
