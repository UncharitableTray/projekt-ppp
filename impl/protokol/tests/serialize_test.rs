extern crate bincode;
extern crate openssl;
extern crate protokol;
extern crate serde;
extern crate sodiumoxide;

use bincode::{deserialize, serialize};
use openssl::bn::BigNum;
use protokol::serdata::advertisement::DataAdvertise;
use protokol::serdata::sharing::{DataShares, EncryptedSerShare, EncryptedShareVec};
use protokol::sss::*;
use sodiumoxide::crypto::box_;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_advertise_round_serialization() {
        let (c_pk, _) = box_::gen_keypair();
        let (s_pk, _) = box_::gen_keypair();
        let weights = vec![0.0; 5];
        let data = DataAdvertise {
            v: 22,
            c_pk: c_pk,
            s_pk: s_pk,
            ws: weights,
        };
        let encoded: Vec<u8> = serialize(&data).unwrap();
        let decoded: DataAdvertise = deserialize(&encoded).unwrap();
        assert_eq!(data, decoded);
    }

    #[test]
    fn test_bignum_serialization() {
        let x = BigNum::from_u32(1234).unwrap();
        let y = BigNum::from_u32(2222).unwrap();
        let share = Share { x: x, y: y };
        let serializable_share = ser_shares::SerializableShare::from_share(&share);
        let encoded: Vec<u8> = serialize(&serializable_share).unwrap();
        let decoded: ser_shares::SerializableShare = deserialize(&encoded).unwrap();
        let decoded_share: Share = decoded.to_share().unwrap();
        assert_eq!(share, decoded_share);
    }

    #[test]
    fn test_share_vec_serialization() {
        let first_data_share = create_data_share(1, 2, 123, 456, 135, 235);
        let second_data_share = create_data_share(2, 1, 434, 534, 213, 352);

        let (fst_pk, fst_sk) = box_::gen_keypair();
        let (snd_pk, snd_sk) = box_::gen_keypair();

        let first_enc_share = EncryptedSerShare::from_share(&first_data_share, &snd_pk, &fst_sk);
        let snd_enc_share = EncryptedSerShare::from_share(&second_data_share, &fst_pk, &snd_sk);

        let encrypted_vec = EncryptedShareVec {
            message: vec![first_enc_share, snd_enc_share],
        };

        let encoded_vec = serialize(&encrypted_vec).unwrap();
        let decoded_vec = deserialize(&encoded_vec).unwrap();

        assert_eq!(encrypted_vec, decoded_vec);

        //Assume user 1 sent the first message to user 2 and vice-versa
        //And now user 2 received the message and wants to decrypt it
        let decoded_first_share = decoded_vec
            .message
            .get(0)
            .unwrap()
            .to_data_share(&fst_pk, &snd_sk);
        let decoded_snd_share = decoded_vec
            .message
            .get(1)
            .unwrap()
            .to_data_share(&snd_pk, &fst_sk);

        assert_eq!(first_data_share, decoded_first_share);
        assert_eq!(second_data_share, decoded_snd_share);
    }

    fn create_data_share(u: usize, v: usize, x1: u32, y1: u32, x2: u32, y2: u32) -> DataShares {
        let x_fst = BigNum::from_u32(x1).unwrap();
        let y_fst = BigNum::from_u32(y1).unwrap();
        let share_fst = Share { x: x_fst, y: y_fst };
        let ss_fst = ser_shares::SerializableShare::from_share(&share_fst);

        let x_snd = BigNum::from_u32(x2).unwrap();
        let y_snd = BigNum::from_u32(y2).unwrap();
        let share_snd = Share { x: x_snd, y: y_snd };
        let ss_snd = ser_shares::SerializableShare::from_share(&share_snd);

        DataShares {
            user_index: u,
            peer_index: v,
            key_share: ss_fst,
            seed_share: ss_snd,
        }
    }
}
