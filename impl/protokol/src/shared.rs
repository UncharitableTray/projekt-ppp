extern crate mio;
extern crate sodiumoxide;

use shared::mio::tcp::TcpStream;
use shared::sodiumoxide::crypto::box_::{Nonce, PrecomputedKey, PublicKey, SecretKey};
use std::collections::VecDeque;

/// Security parameter which defines the maximum secret value size
pub const MAX_SECRET_DEC_STR: &'static str =
    &"29642774844752946028434172162224104410437116074403984394101141506025761187823616";

/// Initial number of clients used in the protocol
pub const NUM_OF_USRS: usize = 6;

/// Minimum number of secret shares needed to reconstruct a secret, it is the threshold for
/// Shamir's secret sharing scheme
pub const SS_THRESHOLD: usize = NUM_OF_USRS/2;

/// Data vector length. Equals to the length of the weight vector + 1.
/// The last element is the cardinality of the client's training examples batch size
pub const DATA_LENGTH: usize = WEIGHT_LEN + 1;

/// The dimension of the weight vector
pub const WEIGHT_LEN: usize = 3;

/// Mio's poll delimiter
pub const POLL_LIMIT: usize = NUM_OF_USRS;

/// Public server mio token as usize
pub const SERVER_TOKEN: usize = 10_000_000;

pub const BUFFER_SIZE: usize = 65536;

pub const EPOCHS: usize = 100;

#[derive(Debug)]
pub struct Peer {
    pub socket: TcpStream,

    pub write_queue: VecDeque<Vec<u8>>,

    pub agreed: bool,

    pub token_index: usize,

    pub peer_precomputed_key: Option<PrecomputedKey>,

    pub nonce: Option<Nonce>,

    pub peer_data: ClientData,
}

#[derive(Clone, Debug)]
pub struct ClientData {
    pub token_index: usize,

    pub c_pub_key: Option<PublicKey>,

    pub c_sec_key: Option<SecretKey>,

    pub s_pub_key: Option<PublicKey>,

    pub s_sec_key: Option<SecretKey>,
}

impl ClientData {
    pub fn new(index: usize) -> ClientData {
        ClientData {
            c_pub_key: None,
            c_sec_key: None,
            s_pub_key: None,
            s_sec_key: None,
            token_index: index,
        }
    }
}
