extern crate base64;
extern crate hex;
extern crate openssl;

pub mod conversions {
    use util::openssl::bn::BigNum;

    pub fn hex_str_to_b64(src: &str) -> String {
        let num = BigNum::from_hex_str(&src).unwrap();
        return base64::encode(&num.to_vec());
    }

    pub fn byte_slice_to_hex(src: &[u8]) -> String {
        hex::encode(src)
    }

    pub fn bytes_to_hex(src: &Vec<u8>) -> String {
        hex::encode(src)
    }

    pub fn bytes_to_string(src: &Vec<u8>) -> String {
        let result: String = src.iter().map(|x| *x as char).collect();
        result
    }

    pub fn str_to_bytes(src: &str) -> Vec<u8> {
        src.to_owned().into_bytes()
    }

    pub fn copy_inplace(dest: &mut [u8], src: &[u8]) -> Option<()> {
        if dest.len() != src.len() {
            return None;
        }
        for (d, s) in dest.into_iter().zip(src.into_iter()) {
            *d = *s;
        }
        Some(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_copy_inplace() {
        let mut dest: [u8; 5] = [1, 2, 3, 4, 5];
        let src: [u8; 5] = [5, 5, 5, 5, 5];
        let src2: [u8; 4] = [5, 4, 3, 2];

        let res1 = conversions::copy_inplace(&mut dest, &src);
        assert_eq!(res1, Some(()));
        assert_eq!(dest, src);

        let res2 = conversions::copy_inplace(&mut dest, &src2);
        assert_eq!(res2, None);
    }

}
