extern crate openssl;
extern crate rand;

use noise_f::openssl::bn::*;
use noise_f::rand::distributions::Uniform;
use noise_f::rand::prelude::*;
use shared::{DATA_LENGTH, MAX_SECRET_DEC_STR};
use std::fmt;
use util::conversions::bytes_to_hex;

type Result<T> = std::result::Result<T, openssl::error::ErrorStack>;

static PRECISION_COEF: f64 = 1e30;

static OFFSET_DECIMAL_STR: &'static str =
    &"14821387422376473014217086081112052205218558037201992197050570753012880593911808";

#[derive(Clone, Debug, PartialEq)]
pub struct IndexedSeed {
    pub u: usize,
    pub v: usize,
    pub seed: Vec<u8>,
}

impl fmt::Display for IndexedSeed {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "IndexedSeed ({} to {} : {})",
            self.u,
            self.v,
            bytes_to_hex(&self.seed)
        )
    }
}

pub fn filter_remaining(
    u2: &[usize],
    u3: &[usize],
    indexed_seeds: &[IndexedSeed],
) -> Vec<IndexedSeed> {
    let diff = u2
        .iter()
        .filter(|x| !u3.contains(&x))
        .cloned()
        .collect::<Vec<usize>>();
    indexed_seeds
        .iter()
        .filter(|s| diff.contains(&s.u) && u3.contains(&s.v))
        .cloned()
        .collect::<Vec<IndexedSeed>>()
}

pub fn noise_generate(
    x: &[f64],
    own_seed: &[u8],
    indexed_seeds: &[IndexedSeed],
) -> Result<Vec<BigNum>> {
    let offset = BigNum::from_dec_str(OFFSET_DECIMAL_STR)?;
    let mut w = data_to_bn(x, &offset).unwrap();
    let r = BigNum::from_dec_str(MAX_SECRET_DEC_STR)?;
    let mut ctx = BigNumContext::new()?;

    w = add_noise(&w, own_seed, &r, &mut ctx)?;
    for ind in indexed_seeds.iter() {
        let delta = ind.v as i32 - ind.u as i32;
        //println!("Own index(u): {}, their index(v): {} = {}", ind.u, ind.v, delta);
        if delta > 0 {
            //println!("Adding");
            w = add_noise(&w, &ind.seed, &r, &mut ctx)?;
        } else if delta < 0 {
            //println!("Subtracting");
            w = sub_noise(&w, &ind.seed, &r, &mut ctx)?;
        }
    }
    Ok(w)
}

pub fn noise_undo(
    ys: &[Vec<BigNum>],
    survivor_seeds: &[Vec<u8>],
    filtered_dropped_seeds: &[IndexedSeed],
) -> Result<Vec<f64>> {
    let zeros = [0.0; DATA_LENGTH];
    let offset = BigNum::from_dec_str(OFFSET_DECIMAL_STR)?;
    let mut w = data_to_bn(&zeros, &offset)?;
    let r = BigNum::from_dec_str(MAX_SECRET_DEC_STR)?;
    let mut ctx = BigNumContext::new()?;

    for y in ys {
        //println!("Adding {:?} to {:?}\n", y, w);
        w = add_vecs(&w, y, &r, &mut ctx)?;
    }

    for s in survivor_seeds.iter() {
        w = sub_noise(&w, s, &r, &mut ctx)?;
    }

    for s in filtered_dropped_seeds.iter() {
        let delta = s.v as i32 - s.u as i32;
        if delta > 0 {
            w = add_noise(&w, &s.seed, &r, &mut ctx)?;
        } else if delta < 0 {
            w = sub_noise(&w, &s.seed, &r, &mut ctx)?;
        }
    }
    Ok(data_from_bn(&w, &offset)?)
}

fn add_vecs(
    w: &Vec<BigNum>,
    y: &Vec<BigNum>,
    r: &BigNum,
    ctx: &mut BigNumContextRef,
) -> Result<Vec<BigNum>> {
    let mut result = Vec::new();
    let len = w.len();
    for i in 0..len {
        let mut res = BigNum::new()?;
        let bn = w.get(i).unwrap();
        let y_val = y.get(i).unwrap();
        res.mod_add(bn, y_val, r, ctx)?;
        result.push(res);
    }
    Ok(result)
}

fn data_to_bn(x: &[f64], offset: &BigNum) -> Result<Vec<BigNum>> {
    let mut w = Vec::new();
    for v in x.iter() {
        //println!("{} * {} = {}", v, PRECISION_COEF, (*v * PRECISION_COEF));
        let mut num_str = (*v * PRECISION_COEF).to_string();
        //println!("Truncated: {}", num_str);
        if num_str.contains('.') {
            let dot_ind = num_str.find('.').unwrap();
            num_str.replace_range(dot_ind.., "");
        }
        let mut num = BigNum::from_dec_str(&num_str)?;
        //println!("Converting {} to {}", v, num);
        //if *v < 0.0 {
        //    println!("Subbing: {}", num);
        //    num = &BigNum::new()? - &num;
        //}
        num = &num + offset;
        //println!("Adding offset: {}", num);
        w.push(num);
    }
    Ok(w)
}

fn data_from_bn(x: &Vec<BigNum>, offset: &BigNum) -> Result<Vec<f64>> {
    let mut w = Vec::new();
    for v in x.iter() {
        let val = v - offset;
        //println!("v {} - offset = {}", v, v - offset);
        let int_str = val.to_dec_str()?.to_string();
        //println!("Parsing number {}", v);
        let f = int_str
            .parse::<f64>()
            .expect("Unable to parse bigint to float.");
        //println!("parsed: {} || divided: {}", f, f / PRECISION_COEF);
        w.push(f / PRECISION_COEF);
    }
    Ok(w)
}

fn add_noise(
    w: &Vec<BigNum>,
    seed: &[u8],
    r: &BigNum,
    ctx: &mut BigNumContextRef,
) -> Result<Vec<BigNum>> {
    let mut result = Vec::new();
    let mut local_seed = [0u8; 32];
    let between = Uniform::from(0..=u8::max_value());

    for (local, outer) in local_seed.iter_mut().zip(seed.iter()) {
        *local = *outer;
    }
    let mut rng = StdRng::from_seed(local_seed);

    for i in 0..w.len() {
        let noise = rng_noise(&mut rng, &between)?;
        let mut bn = w.get(i).unwrap();
        let mut res = BigNum::new()?;
        res.mod_add(bn, &noise, r, ctx)?;
        result.push(res);
    }
    Ok(result)
}

fn sub_noise(
    w: &Vec<BigNum>,
    seed: &[u8],
    r: &BigNum,
    ctx: &mut BigNumContextRef,
) -> Result<Vec<BigNum>> {
    let mut result = Vec::new();
    let mut local_seed = [0u8; 32];
    let between = Uniform::from(0..=u8::max_value());

    for (local, outer) in local_seed.iter_mut().zip(seed.iter()) {
        *local = *outer;
    }
    let mut rng = StdRng::from_seed(local_seed);

    for i in 0..w.len() {
        let noise = rng_noise(&mut rng, &between)?;
        let mut bn = w.get(i).unwrap();
        let mut res = BigNum::new()?;
        res.mod_sub(bn, &noise, r, ctx)?;
        result.push(res);
    }
    Ok(result)
}

fn rng_noise(rng: &mut StdRng, dist: &Uniform<u8>) -> Result<BigNum> {
    let mut bytes = [0 as u8; 32];
    for i in 0..32 {
        bytes[i] = dist.sample(rng);
    }
    let bn = BigNum::from_slice(&bytes)?;
    Ok(bn)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_noise() {
        let xs = create_xs_f64();
        let seeds = create_seeds();
        let indexed_seeds = create_indexed();
        let ys = calculate_ys_f64(&xs, &seeds, &indexed_seeds);
        let sum = noise_undo(&ys, &seeds, &[]).unwrap();
        assert_eq!(&sum[..], [87934.66; DATA_LENGTH]);
    }

    fn create_xs_f64() -> Vec<Vec<f64>> {
        let x1 = vec![0.20471489547322128; DATA_LENGTH];
        let x2 = vec![-0.03262020550192208; DATA_LENGTH];
        let x3 = vec![-0.009186138149439665; DATA_LENGTH];
        let x4 = vec![-0.16829067713291554; DATA_LENGTH];

        let mut res = Vec::new();
        res.push(x1);
        res.push(x2);
        res.push(x3);
        res.push(x4);
        res
    }

    fn create_seeds() -> Vec<Vec<u8>> {
        let mut res = Vec::new();
        res.push(vec![
            11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
        ]);
        res.push(vec![
            22, 33, 44, 55, 66, 77, 88, 99, 11, 22, 33, 44, 55, 66, 77, 88,
        ]);
        res.push(vec![
            13, 24, 42, 52, 32, 43, 76, 78, 54, 75, 75, 43, 21, 53, 12, 19,
        ]);
        res.push(vec![
            131, 241, 142, 152, 132, 143, 176, 178, 154, 175, 175, 143, 221, 153, 212, 219,
        ]);
        res
    }

    fn create_indexed() -> Vec<IndexedSeed> {
        let s12 = IndexedSeed {
            u: 1,
            v: 2,
            seed: vec![1, 2, 3, 4, 5, 6, 7, 8],
        };

        let s21 = IndexedSeed {
            u: 2,
            v: 1,
            seed: vec![1, 2, 3, 4, 5, 6, 7, 8],
        };

        let s13 = IndexedSeed {
            u: 1,
            v: 3,
            seed: vec![11, 22, 33, 44, 55, 66, 77, 88],
        };

        let s31 = IndexedSeed {
            u: 3,
            v: 1,
            seed: vec![11, 22, 33, 44, 55, 66, 77, 88],
        };

        let s14 = IndexedSeed {
            u: 1,
            v: 4,
            seed: vec![6, 6, 6, 6, 6, 6, 6, 6],
        };

        let s41 = IndexedSeed {
            u: 4,
            v: 1,
            seed: vec![6, 6, 6, 6, 6, 6, 6, 6],
        };

        let s23 = IndexedSeed {
            u: 2,
            v: 3,
            seed: vec![0, 21, 32, 43, 54, 65, 76, 87, 97, 98, 12, 4, 2],
        };

        let s32 = IndexedSeed {
            u: 3,
            v: 2,
            seed: vec![0, 21, 32, 43, 54, 65, 76, 87, 97, 98, 12, 4, 2],
        };

        let s24 = IndexedSeed {
            u: 2,
            v: 4,
            seed: vec![10, 121, 32, 43, 54, 65, 76, 87, 97, 98, 12, 4, 2],
        };

        let s42 = IndexedSeed {
            u: 4,
            v: 2,
            seed: vec![10, 121, 32, 43, 54, 65, 76, 87, 97, 98, 12, 4, 2],
        };

        let s34 = IndexedSeed {
            u: 3,
            v: 4,
            seed: vec![0, 21, 32, 43, 54, 165, 176, 87, 97, 98, 12, 4, 2],
        };

        let s43 = IndexedSeed {
            u: 4,
            v: 3,
            seed: vec![0, 21, 32, 43, 54, 165, 176, 87, 97, 98, 12, 4, 2],
        };

        vec![s12, s21, s13, s31, s14, s41, s23, s32, s24, s42, s34, s43]
    }

    fn calculate_ys_f64(
        xs: &[Vec<f64>],
        ps: &[Vec<u8>],
        indexed: &[IndexedSeed],
    ) -> Vec<Vec<BigNum>> {
        let mut ys = Vec::new();

        let y1 = noise_generate(
            xs.get(0).unwrap(),
            ps.get(0).unwrap(),
            &[
                indexed.get(0).unwrap().clone(),
                indexed.get(2).unwrap().clone(),
                indexed.get(4).unwrap().clone(),
            ],
        )
        .unwrap();
        ys.push(y1);

        let y2 = noise_generate(
            xs.get(1).unwrap(),
            ps.get(1).unwrap(),
            &[
                indexed.get(1).unwrap().clone(),
                indexed.get(6).unwrap().clone(),
                indexed.get(8).unwrap().clone(),
            ],
        )
        .unwrap();
        ys.push(y2);

        let y3 = noise_generate(
            xs.get(2).unwrap(),
            ps.get(2).unwrap(),
            &[
                indexed.get(3).unwrap().clone(),
                indexed.get(7).unwrap().clone(),
                indexed.get(10).unwrap().clone(),
            ],
        )
        .unwrap();
        ys.push(y3);

        let y4 = noise_generate(
            xs.get(3).unwrap(),
            ps.get(3).unwrap(),
            &[
                indexed.get(5).unwrap().clone(),
                indexed.get(9).unwrap().clone(),
                indexed.get(11).unwrap().clone(),
            ],
        )
        .unwrap();
        ys.push(y4);
        ys
    }
}
