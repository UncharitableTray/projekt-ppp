extern crate bincode;
extern crate linalg;
extern crate ml;
extern crate openssl;
extern crate sodiumoxide;

use std::collections::HashMap;
use std::io::{Read, Result, Write};
use std::net::{Shutdown, TcpStream};
use std::thread::sleep;
use std::time::Duration;

#[allow(unused_imports)]
use self::bincode::{deserialize, serialize};
use self::linalg::vector::{RealVec, Vector};
use self::openssl::bn::BigNum;
use self::sodiumoxide::crypto::box_;
use self::sodiumoxide::crypto::box_::curve25519xsalsa20poly1305 as asym_ae;

use noise_f::{noise_generate, IndexedSeed};
use serdata::sharing::{DataShares, EncryptedSerShare, EncryptedShareVec};
use serdata::unmasking::{UnmaskingShare, UnmaskingShareVec};
use serdata::*;
use shared::*;
use sss::ser_shares::SerializableShare;
use sss::{ShamirScheme, Share};
use util::conversions;

use self::ml::classif::logreg::LogReg2D;

pub struct Client {
    stream: TcpStream,

    pub_key: box_::PublicKey,

    sec_key: box_::SecretKey,

    precomputed_kp: Option<asym_ae::PrecomputedKey>,

    nonce: Option<asym_ae::Nonce>,

    data: ClientData,

    peer_key_map: Option<HashMap<usize, advertisement::DataAdvertise>>,

    peer_serializable_data: Option<Vec<sharing::DataShares>>,

    encrypted_share_vec: Vec<sharing::EncryptedSerShare>,

    weight_vector: RealVec,

    own_seed: Vec<u8>,

    u2: Vec<usize>,

    u3: Vec<usize>,

    learning: Option<LogReg2D>,
}

impl Client {
    pub fn from_ip(ip: &str, pubkey: box_::PublicKey, seckey: box_::SecretKey) -> Result<Client> {
        let client = Client {
            stream: TcpStream::connect(ip)?,
            pub_key: pubkey,
            sec_key: seckey,
            precomputed_kp: None,
            nonce: None,
            data: ClientData::new(1),
            peer_key_map: None,
            peer_serializable_data: None,
            encrypted_share_vec: Vec::new(),
            weight_vector: RealVec::from_vec(vec![1.0; WEIGHT_LEN]),
            own_seed: Vec::new(),
            u2: Vec::new(),
            u3: Vec::new(),
            learning: None,
        };
        Ok(client)
    }

    pub fn init_secure_channel(&mut self) -> Result<()> {
        let sent = self.stream.write(&self.pub_key.0);
        let bytes = match sent {
            Ok(num_bytes) => num_bytes,
            _ => panic!(
                "Unable to establish a secure connection with the peer: {}",
                self.stream.peer_addr().unwrap()
            ),
        };
        //TODO: cleanup prints
        println!("Sent PK to server ({}B) : {:?}", bytes, self.pub_key.0);

        let mut buf: [u8; 32] = [0u8; 32];
        let _read = self.stream.read(&mut buf)?;
        let server_pub_key = asym_ae::PublicKey(buf);

        let mut buf: [u8; asym_ae::NONCEBYTES] = [0u8; asym_ae::NONCEBYTES];
        let read = self.stream.read(&mut buf)?;
        let nonce = match asym_ae::Nonce::from_slice(&buf) {
            Some(n) => n,
            _ => panic!("Unable to reconstruct the nonce. Aborting!"),
        };
        //println!("Nonce is {}", conversions::byte_slice_to_hex(&nonce.0));
        self.nonce = Some(nonce);
        println!("Received message ({}B): {:?}", read, buf);

        let precomputed_key = asym_ae::precompute(&server_pub_key, &self.sec_key);
        println!(
            "Client precomputed key: {}",
            conversions::byte_slice_to_hex(&precomputed_key.0)
        );
        self.precomputed_kp = Some(precomputed_key);
        Ok(())
    }

    pub fn init_data(&mut self) {
        let (c_pub_k, c_sec_k) = box_::gen_keypair();
        let (s_pub_k, s_sec_k) = box_::gen_keypair();
        //println!("S_SK {}", conversions::byte_slice_to_hex(&s_sec_k.0));
        self.data.c_pub_key = Some(c_pub_k);
        self.data.c_sec_key = Some(c_sec_k);
        self.data.s_pub_key = Some(s_pub_k);
        self.data.s_sec_key = Some(s_sec_k);
    }

    pub fn is_empty_nonce(&self) -> bool {
        let empty = [0u8; asym_ae::NONCEBYTES];
        let nonce = self.nonce.unwrap();
        nonce.0 == empty
    }

    pub fn is_empty_key(&self) -> bool {
        let empty = [0u8; 32];
        let ref key = match self.precomputed_kp {
            Some(ref k) => k,
            _ => panic!("Precomputed key does not exist."),
        };
        key.0 == empty
    }

    fn phase_advertise_keys(&mut self) -> Result<()> {
        self.receive_all_users()?;
        Ok(())
    }

    fn register(&mut self) -> Result<()> {
        let mut buf = [0u8; 64];
        let (c_pk, s_pk) = (
            self.data.c_pub_key.unwrap().0,
            self.data.s_pub_key.unwrap().0,
        );
        //println!("c_pk: {}", conversions::byte_slice_to_hex(&c_pk));
        //println!("s_pk: {}", conversions::byte_slice_to_hex(&s_pk));
        for (index, x) in c_pk.iter().chain(s_pk.iter()).enumerate() {
            buf[index] = *x;
        }
        let bytes = self.send_encrypted(&buf);

        match bytes {
            Ok(_) => return Ok(()),
            _ => panic!("Error while advertising keys: {:?}", bytes),
        };
    }

    fn receive_all_users(&mut self) -> Result<()> {
        let plaintext = self.read_encrypted()?;
        let all_data: Vec<advertisement::DataAdvertise> = match deserialize(&plaintext) {
            Ok(d) => d,
            Err(e) => panic!(
                "Unable to deserialize data in the advertise keys round. Error: {}",
                e
            ),
        };

        {
            let fst = all_data.get(0).unwrap();
            let weight_vec = fst.ws.iter().cloned().collect::<Vec<f64>>();
            self.weight_vector = RealVec::from_vec(weight_vec);
            let weight_vec = fst.ws.iter().cloned().collect::<Vec<f64>>();
            self.learning = Some(LogReg2D::from_weights_simulation(
                RealVec::from_vec(weight_vec),
                Some(&vec![1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16])
            ));
        }

        if all_data.len() < SS_THRESHOLD {
            self.terminate_stream()?;
            panic!("Not enough users remaining in the protocol. Aborting!");
        }

        self.update_own_index(&all_data);
        let own_index = self.data.token_index;
        let peer_data: Vec<advertisement::DataAdvertise> = all_data
            .iter()
            .filter(|p| p.v != own_index)
            .map(|x| x.clone())
            .collect();

        //for peer in peer_data.iter() {
        //println!("{}", peer);
        //}

        let mut peer_map = HashMap::new();
        for peer in peer_data.iter() {
            peer_map.insert(peer.v, peer.clone());
        }
        self.peer_key_map = Some(peer_map);
        Ok(())
    }

    fn update_own_index(&mut self, peer_data: &Vec<advertisement::DataAdvertise>) {
        let ref own_c_pk = self.data.c_pub_key.as_ref().unwrap();
        let ref own_s_pk = self.data.s_pub_key.as_ref().unwrap();

        for peer in peer_data.iter() {
            let ref their_c_pk = peer.c_pk;
            let ref their_s_pk = peer.s_pk;
            if their_c_pk.0 == own_c_pk.0 && their_s_pk.0 == own_s_pk.0 {
                self.data.token_index = peer.v;
                break;
            }
        }
        println!("This client's index is: {}", self.data.token_index);
    }

    fn phase_send_shares(&mut self) -> Result<()> {
        let share_vec = self.generate_encrypted_share_vec()?;

        //println!("Sending secret share vector to the server.");
        let encoded =
            serialize(&share_vec).expect("Unable to serialize secret shares before sending.");
        match self.send_encrypted(&encoded[..]) {
            Ok(_) => return Ok(()),
            Err(e) => panic!("Unable to send encrypted share vec: {}", e),
        };
    }

    fn generate_encrypted_share_vec(&mut self) -> Result<EncryptedShareVec> {
        let field_prime = BigNum::from_dec_str(MAX_SECRET_DEC_STR)?;
        let shamir_scheme = ShamirScheme::from_bn(&field_prime, NUM_OF_USRS, SS_THRESHOLD)
            .expect("Unable to initialize the secret sharing scheme. Aborting!");

        let mut own_rng_seed = BigNum::new()?;
        own_rng_seed.generate_prime(256, true, None, None)?;
        self.own_seed = own_rng_seed.to_vec();
        //println!("User {}'s seed is {}", self.data.token_index, conversions::bytes_to_hex(&self.own_seed));
        let mut seed_shares = match shamir_scheme.gen_shares(&own_rng_seed) {
            Ok(s) => s,
            Err(e) => panic!("Unable to generate shares of private seed: {}", e),
        };

        let secret_key_as_num = BigNum::from_slice(&self.data.s_sec_key.as_ref().unwrap().0)?;

        //println!("Now generating key shares!");
        let mut key_shares = match shamir_scheme.gen_shares(&secret_key_as_num) {
            Ok(s) => s,
            Err(e) => panic!("Unable to generate shares of private key: {}", e),
        };

        if seed_shares.len() != key_shares.len() {
            panic!("Differing number of key and seed shares. Aborting!");
        }

        let serializable_shares = self.shares_to_serializable(&mut seed_shares, &mut key_shares)?;
        //println!("Shares generated: {}", serializable_shares.len());

        let encrypted_shares = self.encrypt_serializable(&serializable_shares);
        //println!("Shares encrypted and packed into a vector.");

        self.peer_serializable_data = Some(serializable_shares);
        Ok(encrypted_shares)
    }

    fn encrypt_serializable(&self, shares: &Vec<DataShares>) -> EncryptedShareVec {
        let own_sk = self.data.c_sec_key.as_ref().unwrap();
        let others = self.peer_key_map.as_ref().unwrap();

        let enc_shares = shares
            .iter()
            .map(|share| {
                let peer_data = others
                    .get(&share.peer_index)
                    .expect("No peer public key found. Aborting");
                let their_pk = peer_data.c_pk;
                EncryptedSerShare::from_share(share, &their_pk, own_sk)
            })
            .collect::<Vec<EncryptedSerShare>>();

        EncryptedShareVec {
            message: enc_shares,
        }
    }

    fn shares_to_serializable(
        &mut self,
        seed_shares: &mut Vec<Share>,
        key_shares: &mut Vec<Share>,
    ) -> Result<Vec<sharing::DataShares>> {
        let own_index = self.data.token_index;
        let serializable_shares = self
            .peer_key_map
            .as_ref()
            .unwrap()
            .values()
            .map(|p| {
                let seed_share = seed_shares
                    .pop()
                    .expect("Error! Not enough shares. Aborting.");
                let seed_share = SerializableShare::from_share(&seed_share);
                let key_share = key_shares
                    .pop()
                    .expect("Error! Not enough shares. Aborting.");
                let key_share = SerializableShare::from_share(&key_share);

                let new_share = sharing::DataShares {
                    user_index: own_index,
                    peer_index: p.v,
                    key_share: key_share,
                    seed_share: seed_share,
                };
                new_share
            })
            .collect::<Vec<sharing::DataShares>>();
        Ok(serializable_shares)
    }

    fn phase_masked_input_collection(&mut self) -> Result<()> {
        self.collect_shares()?;
        let seeds = self.compute_seeds()?;
        let y;
        println!("Masking values: {:?}", self.weight_vector);
        {
            let ref own_seed = self.own_seed;

            let learning = self.learning.as_mut().unwrap();
            let examples_len = learning.examples_len();
            //let epochs = 1;

            let gradient = learning.calculate_gradient_batch(examples_len)
                .expect("Unable to calculate my own gradient for some reason.");
            println!("Own gradient is: {}\nAfter scaling: {}", gradient, gradient.scale(1.0/examples_len as f64));
            let mut x = gradient.values().iter().cloned().collect::<Vec<f64>>();
            x.push(examples_len as f64);
            y = noise_generate(&x[..], own_seed, &seeds).expect("Unable to generate noise.");
            //println!("Masked gradient is: {:?}\n", y);
        }

        let y = y.iter().map(|x| x.to_vec()).collect::<Vec<Vec<u8>>>();
        let encoded_y = serialize(&y).expect("Unable to encode y");
        self.send_encrypted(&encoded_y)?;

        let u3 = self.receive_u3()?;
        if u3.len() < SS_THRESHOLD {
            panic!("Not enough users remaining. Aborting!");
        }
        //println!("Remaining users are: {:?}", u3);
        self.u3 = u3;
        Ok(())
    }

    fn collect_shares(&mut self) -> Result<()> {
        let encoded_vector = self.read_encrypted().unwrap();
        let enc_share_vec: sharing::EncryptedShareVec = deserialize(&encoded_vector)
            .expect("Unable to deserialize the encoded vector of encrypted shares.");

        self.calculate_u2(&enc_share_vec);

        //println!("Read {} cyphertexts. Storing.", enc_share_vec.message.len());
        self.encrypted_share_vec = enc_share_vec.message;
        Ok(())
    }

    fn compute_seeds(&mut self) -> Result<Vec<IndexedSeed>> {
        let mut seeds = Vec::new();
        let ref my_sec_key = self.data.s_sec_key.as_ref().unwrap();
        let ref key_map = self.peer_key_map.as_ref().unwrap();
        let ref others = self.u2;

        for v in others.iter() {
            if *v == self.data.token_index {
                continue;
            }
            let their_data = match key_map.get(v) {
                Some(d) => d,
                None => panic!("No data found for user token {}", v),
            };
            let their_s_pk = their_data.s_pk;
            let precomputed = box_::precompute(&their_s_pk, my_sec_key);

            let indexed_seed = IndexedSeed {
                u: self.data.token_index,
                v: *v,
                seed: Vec::from(&precomputed.0[..]),
            };
            //println!("Generated seed: {}", indexed_seed);
            seeds.push(indexed_seed);
        }

        Ok(seeds)
    }

    fn calculate_u2(&mut self, shares: &EncryptedShareVec) {
        //println!("Share len: {}", shares.message.len());
        let mut users = shares.message.iter().map(|m| m.v).collect::<Vec<usize>>();

        users.sort_unstable();
        users.dedup();
        //println!("Users: {:?}", users);
        if users.len() < SS_THRESHOLD {
            self.terminate_stream().unwrap();
            panic!("Number of clients is lower than the threshold. Aborting!")
        }

        self.u2 = users;
    }

    fn receive_u3(&mut self) -> Result<Vec<usize>> {
        let encoded = self.read_encrypted()?;
        let u3: Vec<usize> = deserialize(&encoded).expect("Unable to deserialize u3");
        Ok(u3)
    }

    fn phase_unmasking(&mut self) -> Result<()> {
        let ref peers = self
            .u2
            .iter()
            .filter(|p| **p != self.data.token_index)
            .cloned()
            .collect::<Vec<usize>>();
        let mut share_map = HashMap::new();
        {
            let own_index = self.data.token_index;
            let ref mut enc_shares = self.encrypted_share_vec;
            let mut enc_shares: Vec<EncryptedSerShare> = enc_shares
                .iter()
                .filter(|s| s.u == own_index || s.v == own_index)
                .cloned()
                .collect();
            let ref key_map = self.peer_key_map.as_ref().unwrap();

            let ref own_sec_key = self
                .data
                .c_sec_key
                .as_ref()
                .expect("Own secure key missing.");

            for v in peers.iter() {
                let mut share;
                let mut index = 0;
                let len = enc_shares.len() + 1;
                for i in 0..len {
                    let s = match enc_shares.get(i) {
                        Some(x) => x,
                        None => panic!("No share for user {}", v),
                    };
                    if s.u == *v {
                        index = i;
                        break;
                    }
                }
                share = enc_shares.swap_remove(index);

                let their_keys = match key_map.get(v) {
                    Some(k) => k,
                    None => panic!(
                        "Client {}: No keys found for user {}",
                        self.data.token_index, v
                    ),
                };
                let their_c_pk = their_keys.c_pk;
                let nonce = asym_ae::Nonce::from_slice(&share.nonce[..])
                    .expect("Unable to reconstruct the nonce.");

                let decrypted =
                    match asym_ae::open(&share.ciphertext, &nonce, &their_c_pk, own_sec_key) {
                        Ok(d) => d,
                        Err(e) => panic!(
                            "Unable to decrypt the share. {} {} using nonce {}. Full error: {:?}",
                            share.u,
                            share.v,
                            conversions::byte_slice_to_hex(&nonce.0),
                            e
                        ),
                    };
                //println!("Decrypted share {} to {}", *v, self.data.token_index);

                let dec_share =
                    deserialize(&decrypted).expect("Unable to deserialize decrypted share");
                share_map.insert(v, dec_share);
            }
            assert_eq!(share_map.values().len(), self.u2.len() - 1);
        }
        let share_vec = self.generate_decrypted_share_vec(&share_map)?;
        self.send_decrypted_share_vec(&share_vec)?;
        Ok(())
    }

    fn generate_decrypted_share_vec(
        &mut self,
        share_map: &HashMap<&usize, DataShares>,
    ) -> Result<UnmaskingShareVec> {
        let mut dec_shares = Vec::new();
        let dropouts: Vec<usize> = self
            .u2
            .iter()
            .filter(|x| !self.u3.contains(x))
            .cloned()
            .collect();

        share_map
            .values()
            .map(|s| {
                let send_key = dropouts.contains(&s.user_index);
                UnmaskingShare {
                    owner_index: s.user_index,
                    peer_index: s.peer_index,
                    is_key_share: send_key,
                    share: match send_key {
                        true => s.key_share.clone(),
                        false => s.seed_share.clone(),
                    },
                }
            })
            .for_each(|x| dec_shares.push(x));
        Ok(UnmaskingShareVec { shares: dec_shares })
    }

    fn send_decrypted_share_vec(&mut self, shares: &UnmaskingShareVec) -> Result<()> {
        let encoded = serialize(&shares).expect("Unable to serialize the share vector");
        let _ = self.send_encrypted(&encoded)?;
        //println!("Sent {}B of encoded share vector", bytes);
        Ok(())
    }

    /// Implements sending encrypted messages using public-key AE with precomputed keys.
    fn send_encrypted(&mut self, plaintext: &[u8]) -> Result<usize> {
        let nonce = self.nonce.as_mut().unwrap();
        let key = match &self.precomputed_kp {
            Some(k) => k,
            None => panic!("No precomputed key found towards peer. Aborting."),
        };
        //println!("Encrypting with nonce {}", conversions::byte_slice_to_hex(&nonce.0));
        let ciphertext = box_::seal_precomputed(&plaintext, &nonce, &key);
        let bytes = self
            .stream
            .write(&ciphertext)
            .expect("Unable to send all of the data.");
        nonce.increment_le_inplace();
        Ok(bytes)
    }

    fn read_encrypted(&mut self) -> Result<Vec<u8>> {
        let mut buf = [0u8; BUFFER_SIZE];
        let ref mut nonce = self.nonce.as_mut().unwrap();
        let mut ciphertext = Vec::new();

        let key = match self.precomputed_kp {
            Some(ref key) => key,
            _ => panic!("No precomputed key provided for the server."),
        };
        loop {
            let mut peek_buf = [0; 1];
            let _ = match self.stream.peek(&mut peek_buf) {
                Ok(a) => a,
                _ => break,
            };
            let bytes = self.stream.read(&mut buf)?;
            //println!("Read {} bytes encrypted", bytes);
            //println!("Decrypting with nonce {}", conversions::byte_slice_to_hex(&nonce.0));
            ciphertext.append(&mut buf[0..bytes].into_iter().map(|x| *x).collect::<Vec<u8>>());
            if bytes < BUFFER_SIZE {
                break;
            }
        }
        let plaintext = match asym_ae::open_precomputed(&ciphertext, &nonce, key) {
            Ok(m) => m,
            _ => panic!(
                "Unable to decrypt and/or verify the contents of the message received from the server decrypted by key {}",
                    conversions::byte_slice_to_hex(&key.0))
        };
        //println!("Received message: {}", conversions::byte_slice_to_hex(&plaintext[..]));
        nonce.increment_le_inplace();
        Ok(plaintext)
    }

    pub fn terminate_stream(&mut self) -> Result<()> {
        self.stream.shutdown(Shutdown::Both)
    }

    pub fn initialize(&mut self) -> Result<()> {
        self.init_data();
        sleep(Duration::from_millis(NUM_OF_USRS as u64 * 100));
        self.register()?;
        Ok(())
    }

    pub fn run_round(&mut self) -> Result<()> {
        self.phase_advertise_keys()?;
        println!("Advertised data");
        sleep(Duration::from_millis(NUM_OF_USRS as u64 * 1));
        self.phase_send_shares()?;
        println!("Sent shares");
        sleep(Duration::from_millis(NUM_OF_USRS as u64 * 1));
        self.phase_masked_input_collection()?;
        println!("Collected masked input");
        sleep(Duration::from_millis(NUM_OF_USRS as u64 * 1));
        self.phase_unmasking()?;
        println!("Unmasking done");
        sleep(Duration::from_millis(NUM_OF_USRS as u64 * 1));
        Ok(())
    }
}