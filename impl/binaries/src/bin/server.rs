extern crate mio;
extern crate protokol;
extern crate slab;
extern crate sodiumoxide;

use std::io::Result;

use protokol::server::{Connections, Server};
use protokol::shared::EPOCHS;
use sodiumoxide::crypto::box_;

fn main() -> Result<()> {
    sodiumoxide::init().unwrap();

    let (server_pub_key, server_sec_key) = box_::gen_keypair();
    let mut conns = Connections::from_ip(&"127.0.0.1:8000")?;
    let mut server = Server::new(server_pub_key, server_sec_key)?;
    conns.init()?;
    server.initialize(&mut conns)?;

    for _ in 0..EPOCHS {
        let grad_as_vec = server
            .run_protocol_iter(&mut conns)
            .expect("No value calculated?");
        println!("SUM: {:?}", grad_as_vec);
        println!("Previous weights: {}", server.get_weights());
        server.update_weights(grad_as_vec)?;
        println!("Updated weights: {}", server.get_weights());
    }
    server.assess_model();
    println!("Server: Job's done!");
    Ok(())
}
