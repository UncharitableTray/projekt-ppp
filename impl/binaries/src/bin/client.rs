extern crate protokol;
extern crate sodiumoxide;

use std::io::Result;
use std::thread::sleep;
use std::time::Duration;

use protokol::client::Client;
use protokol::shared::EPOCHS;
use sodiumoxide::crypto::box_;

fn main() -> Result<()> {
    sodiumoxide::init().unwrap();

    sleep(Duration::from_millis(1000));
    let (pubkey, seckey) = box_::gen_keypair();
    println!("Generated keypair");
    let mut client = Client::from_ip(&"127.0.0.1:8000", pubkey, seckey)?;
    println!("Created client");
    client.init_secure_channel()?;
    println!("Created secure channel");
    if client.is_empty_nonce() || client.is_empty_key() {
        println!("There are enough peers already. Aborting");
        client.terminate_stream()?;
        return Ok(());
    }
    client.initialize()?;

    for _ in 0..EPOCHS {
        println!("Running iter");
        client.run_round()?;
    }
    Ok(())
}
