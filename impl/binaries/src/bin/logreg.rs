extern crate ml;
extern crate linalg;

use ml::classif::logreg::{LogReg2D};
use linalg::vector::{Vector, RealVec};

fn main() {
    let mut model = LogReg2D::from_weights_simulation(
        RealVec::from_vec(vec![0.0, 0.0, 0.0]),
        Some(&vec![1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16])
    );
    let eta = 0.1;
    model.plot_dataset();

    for i in 0..1000 {
        let mut gradient = model.calculate_gradient_batch(200).unwrap();
        println!("Gradient: {}", gradient);
        gradient.scale_inplace(1.0/200.0);

        gradient.scale_inplace(eta);
        let new_weights = model.weights().sub(&gradient).unwrap();
        println!("Iter: {}  ---  Scaled gradient: {}  ---  weights: {}", i+1, gradient, new_weights);

        model = LogReg2D::from_weights_simulation(
            new_weights,
            Some(&vec![1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16])
        );
    }
    //model.grad_descent_batch();
    println!("Weights after learning: {}", model.weights());
    let acc = model.validate();
    println!("Accuracy is: {}", acc);
}
