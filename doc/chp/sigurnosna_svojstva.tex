\label{ch:svojstva}
Neformalno, cilj MPC protokola je omogućiti sudionicima da izračunaju točan izlaz neke funkcije, pri čemu su ulazi te funkcije neke vrijednosti koje sudionici žele sačuvati privatnima. Ovakav opis, međutim, nije dovoljan kako bi se takvo svojstvo dokazalo. Umjesto toga, koristi se \textit{realno-idealna paradigma} (engl. \textit{real-ideal paradigm}).

\section{Realno-idealna paradigma}
\label{sec:real-ideal}
Realno-idealna paradigma podrazumijeva podjelu na "stvarni svijet" i "idealni svijet". U toj podjeli, "idealni svijet" implicitno obuhvaća sve sigurnosne zahtjeve i njihove garancije, i sigurnost samog protokola definira se u odnosu na taj idealni svijet. S druge strane, stvarni svijet je onaj u kojemu je protokol implementiran. Cilj protokola je omogućiti sigurnost u stvarnom svijetu koja je ekvivalentna sigurnosti u idealnom, uz neke pretpostavke koje su opisane u nastavku.

\subsection{Idealni svijet}
\label{ssec:idealnisvijet}
U idealnom svijetu, \(n\) sudionika računa funkciju \(\mathcal{F}\) tako da svaki sudionik \(P_i\) neovisno i privatno šalje svoje ulazne vrijednosi \(x_i\) pouzdanoj trećoj strani \(\mathcal{T}\). \(\mathcal{T}\) potom računa \(\mathcal{F}(x_1, x_2, \ldots, x_n)\) i vraća rezultat svim sudionicima. U idealnom svijetu, suparnik bi mogao preuzeti kontrolu nad bilo kojim od sudionika \(P_i\) (ali ne i \(\mathcal{T}\)). Međutim, to ne utječe na količinu informacija
koju suparnik time dobiva, jer su mu i u tom slučaju poznati samo njegov vlastiti \(x_i\) i rezultat izračuna \(\mathcal{F}(x_1, x_2, \ldots, x_n)\).

Također je bitno napomenuti da se idealni svijet zove tako jer se pretpostavlja postojanje pouzdane treće strane \(\mathcal{T}\)\subsection{Stvarni svijet}
\label{ssec:stvarnisvijet}
U stvarnom svijetu ne postoji takav \(\mathcal{T}\). Umjesto toga, svi sudionici komuniciraju koristeći protokol \(\pi\). On za svakoj sudionika \(P_i\) određuje funkciju "sljedeće poruke" (engl. \textit{"next-message" function}) \(\pi_i\). Ona kao ulaz prima neki sigurnosni parametar, pripadajuću ulaznu vrijednost \(x_i\), nasumičnu vrpcu (engl. \textit{random tape}), i popis prethodnih poruka koje je \(P_i\) dosad primio. Kao izlaz, \(\pi_i\) vraća ili sljedeću poruku
i odredište kojemu \(\pi_i\) tu poruku šalje, ili naredbu da se postupak prekida\cite{MPCIntro2018}.

U stvarnom svijetu, suparnik može tijekom provođenja protokola pokvariti sudionika (formalno, sudionik pokvaren od početka je ekvivalentan tome da suparnik sudjeluje u protokolu). Dalje, suparnik može pratiti protokol ili proizvoljno odstupati od njega.
Cilj protokola je, uz određene pretpostavke, u stvarnom svijetu ponuditi sigurnosna svojstva koja su ekvivalentna sigurnosnim svojstvima u idealnom svijetu\cite{MPCIntro2018}.

\section{Pretpostavke o modelu}
\label{sec:pretpostavke}
Budući da implementirani protokol spada u kategoriju MPC protokola, moguće je koristiti prethodno opisani formalni model kako bi se pokazala njegova sigurnosna svojstva. Prije toga, bitno je napomenuti da se podrazumijeva sigurnost korištenih primitiva u implementaciji. Drugim riječima, pretpostavlja se da suparnik neće biti neki aktor koji ne sudjeluje u protokolu nego barem jedan od sudionika, bilo klijent ili server.

\subsection{Sigurnost uz polu-iskrene protivnike}
\label{ssec:semihonest}
U slučaju da suparnik ne odstupa od protokola, zove ga se polu-iskrenim suparnikom (engl. \textit{semi-honest adversary, honest-but-curious}\cite{MPCIntro2018}). On kvari sudionike tako da pritom prate specifikacije protokola. Pritom im je cilj dobiti što više informacija iz poruka koje dobiju od ostalih sudionika. Ovo također uključuje situaciju kada se više pokvarenih sudionika udružuje. Za ovakvog suparnika se također može reći da je pasivan\cite{MPCIntro2018}.

Pokazuje se da je implementirani protokol siguran uz pretpostavku polu-iskrenih suparnika ako se polu-iskreni server udruži s manje od \(t\) (gdje je \(t\) prag Shamirove sheme) polu-iskrenih klijenata\cite{Bonawitz2017}.

Intuitivno, ako manje od \(t\) klijenata udruži svoje poznate podatke, oni ili server neće moći rekonstruirati sve zajedničke i osobne \(seedove\). Međutim, ako ih je \(t\) ili više, onda je moguće iz izraza \ref{eq:reconstruct} dobiti ulazne vektore \(\mathbf{x_u}\) pojedinih
klijenata jer su u toj situaciji poznati \(svi\) osobni i zajednički \(seedovi\). Ova sigurnost garantirana je korištenjem Shamirove sheme i time što svaki klijent u fazi rekonstrukcije bira hoće li poslati dio zajedničkog ili osobnog \(seeda\) \textit{isključivo}. 

\subsection{Sigurnost uz aktivne protivnike}
\label{ssec:malicious}
S druge strane, suparnik koji odstupa od protokola zove se \textit{maliciozni} ili \textit{aktivni} suparnik (engl. \textit{malicious adversary, active adversary}\cite{MPCIntro2018}). On ima sve mogućnosti pasivnog suparnika, s tim da se također može ponašati proizvoljno tijekom izvođenja protokola. Za razliku od pasivnog suparnika, aktivni suparnik također može utjecati na rezultat funkcije \(\mathcal{F}\) \cite{MPCIntro2018}.

Za implementirani protokol, dokazana je samo privatnost ulaznih vrijednosti u slučaju aktivnog suparnika. Puno je teže dokazati točnost i dostupnost protokola u slučaju aktivnog suparnika jer oni lagano mogu slati iskrivljene ulazne vrijednosti, nevaljane dijelove tajni i sl. Pokazuje se da je privatnost svih ulaznih vrijednosti garantirana kada bilo koji broj klijenata surađuje bez servera. Međutim, ako je server također maliciozan, onda je privatnost garantirana samo ako
server surađuje s \(\lceil{\frac{n}{3}}\rceil - 1\) malicioznih ili pasivnih klijenata. Također, u slučaju malicioznog servera, da bi navedene garancije postojale, potrebno je dodati još jednu fazu protokola prije faze rekonstrukcije koja korištenjem infrastrukture javnih ključeva ojačava sigurnost protokola i u skladu s tim je potrebno modificirati preostale faze. Cijeli postupak je detaljno objašnjen u \cite{Bonawitz2017}.
