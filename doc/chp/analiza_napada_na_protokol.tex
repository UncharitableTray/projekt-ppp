\label{chp:napadi}
U odsječku \ref{ssec:malicious} govori se o tome kako protokol, pod pretpostavkom aktivnog suparnika, samo garantira sigurnost ulaznih vektora \(\mathbf{x}_u\) jer on ne mora pratiti protokol nego se može ponašati proizvoljno. U tom slučaju, bitno je analizirati načine na koje bi mogao narušiti stabilnost protokola i točnost rezultata funkcije \(\mathcal{F}\).

\section{Otpornost na bizantske ispade}
\label{sec:bizantski}
Jedan način na koji suparnik može narušavati stabilnost protokola je slati iskrivljene vrijednosti u bilo kojoj fazi. To bi uključivalo, na primjer, netočne vrijednosti dijelova tajni ili slanje znakovnih nizova umjesto numeričkih vrijednosti. Drugi način bio bi slanje krivih, ciljano mijenjanih ulaznih vrijednosti \(\mathbf{x}_u\) u namjeri da se rezultat funkcije \(\mathcal{F}\) promijeni u neku nasumičnu ili cijano pogrešnu vrijednost. Time bi dobiveni model mogao
krivo klasificirati neke primjere ili uopće ne funkcionirati \cite{Bhagoji2018}. Opisane pogreške nazivaju se
\textit{bizantske pogreške} ili \textit{bizantski ispadi} (engl. \textit{Byzantine fault}).

Naziv "bizantska pogreška" dolazi iz alegorične ideje "problema bizantskih generala". U tom problemu, neki broj generala mora surađivati tako da komuniciraju isključivo putem glasnika i svi odani generali moraju donijeti istu odluku. Pritom ih generali izdajnici pokušavaju navesti na krive odluke \cite{Lamport1982}, i njih se zove \textit{bizantski aktori} (engl. \textit{Byzantine worker}). Ova alegorija koristi se u kontekstu raspodijeljenih računalnih sustava kako bi se opisale komponente za koje je teško odrediti jesu li ispravne ili ne. Tada ostale komponente trebaju zajedno odlučiti je li komponenta neispravna ili nije.

Budući da se takvi sustavi često koriste u raspodijeljenom ili zajedničkom učenju, bilo je potrebno provjeriti otpornost takvih sustava na bizantske ispade. Ispostavlja se da bilo koja varijanta protokola koja uzima linearnu kombinaciju gradijenata nije otporna na bizantske ispade. Umjesto toga, predlaže se nova varijanta stohastičkog gradijentnog spusta \textit{Krum} koja pokušava riješiti taj problem \cite{Blanchard2017}. Međutim, pokazuje se da to nije dovoljno
\cite{Bhagoji2018}, jer i
dalje ostaju neke klase napada koje su opisane u nastavku.

\section{Nevidljivost napadača}
Jedan od pristupa kojima se pokušava povećati otpornost na aktivne suparnike je provjeravanje kako pojedini gradijent utječe na performanse modela. Ovo se može ostvariti na dva načina.

Prvi način uspoređuje ponašanje trenutnog modela i modela dobivenog nakon oduzimanja gradijenta. Ako je preciznost novog modela puno lošija od preciznosti modela dobivenog agregacijom svih ostalih gradijenata, onda se taj novi gradijent može malicioznim. Pritom se definira neki proizvoljni prag preciznosti. Međutim, ako je prag visok, onda se propuštaju potencijalne maliciozne vrijednosti, a ako je previše nizak, onda se konvergencija modela može usporiti jer se počinju
odbijati i gradijenti iskrenih korisnika \cite{Bhagoji2018}.

Drugi način uspoređuje koliko se pojedini gradijenti razlikuju od svih ostalih. Pritom je potrebno pažljivo odabrati mjeru kojom se ta razlika mjeri, i ponovno je potrbeno definirati neki prag, ovaj put nad izmjerenim razlikama \cite{Bhagoji2018}.

Ove mjere mogu pomoći u detekciji malicioznih gradijenata u nekim drugim protokolima zajedničkog učenja, ali ne i u ovome. Naime, obje metode temelje se na uspoređivanju novih gradijenata, ali implementirani protokol garantira sigurnost pojedinih gradijenata. Zato je moguće samo provjeriti narušava li suma svih gradijenata zahtjev iz prve metode. Informacija o tome je li pojedini gradijent maliciozan, ili koji korisnik šalje maliciozne gradijente, potpuno se gubi. Kako su sve
vrijednosti sigurne, drugu metodu uopće nije moguće koristiti.

\section{Trovanje podataka}
Trovanje podataka je općenit napad na modele strojnog učenja. Dijeli se na napad \textit{čistim oznakama} (engl. \textit{clean-label}, \cite{Koh2017, Biggio2017}) gdje suparnik ne može mijenjati oznake primjera, i na napad \textit{prljavim oznakama} (engl. \textit{dirty-label}, \cite{Chen2017, Gu2017, Liu2017}) gdje se oznake mogu mijenjati proizvoljno. Pokazuje se da napad prljavim oznakama može postići pogrešnu klasifikaciju uz visoku pouzdanost dodavanjem samo 50
otrovanih primjera \cite{Chen2017}.

U zajedničkom učenju, suparnik bi mogao lokalno generirati otrovane primjere i trenirati model na njima pa poslati maliciozni gradijent. Međutim, kako se njegov gradijent uzima samo kao dio prosjeka svih gradijenata, neće imati velik utjecaj na konačni rezultat pa će ovaj napad biti potpuno neefikasan. Umjesto toga, pokazuje se da je trovanje modela puno efikasniji pristup \cite{Bhagoji2018}.

\section{Trovanje modela}
Napad trovanjem podataka temelji se na trovanju dijela \textit{podataka} koji se koriste prilikom treniranja. S druge strane, trovanje modela izravno mijenja \textit{gradijente} koje klijent šalje serveru. Kako bi se pokazala efikasnost napada uz otežane uvjete napada, podrazumijeva se da suparnik upravlja samo \textit{jednim} malicioznim klijentom \(m\), i njegovim otrovanim (\(\mathcal{D}_m\)) i pomoćnim (\(\mathcal{D}_{aux}\)) podacima \cite{Bhagoji2018}. Originalno se radi otežavanja napada
pretpostavlja da je lakše razlikovati gradijente pojedinih klijenata zbog distribucije njihovih podataka \cite{Bhagoji2018}, ali se u protokolu koji nudi privatnost ulaza taj uvjet zanemaruje.

Cilj suparnika je osigurati da se podaci iz skupa \(\mathcal{D}_{aux}\) krivo klasificiraju kada je postupak treniranja gotov. Pritom je potrebno da model ostale primjere klasificira točno kako bi se validacija modela provela uspješno, čime se smanjuje vjerojatnost detekcije namjernih pogreški u modelu. Formalnije, \(\mathcal{D}_{aux} = \{(\mathbf{x}_i, y_i)\}_{i = 1}^{r}\) i svaki primjer \((\mathbf{x}_i, y_i)\) cilj je klasificirati kao \((\mathbf{x}_i,
\tau_i)\), gdje je \(\tau\) neka druga oznaka iz skupa oznaka.

Prilikom lokalne optimizacije, sve prave oznake \(y_i\) mijenjaju se oznakama \(\tau_i\). Nakon toga, dobiveni gradijent se skalira nekim relativno velikim skalarom \(\lambda\) kojim se negira učinak skaliranja prilikom računanja prosjeka i kojeg je moguće točno izračunati. Ovaj postupak se naziva \textit{eksplicitno pojačavanje} (engl. \textit{explicit boosting}) \cite{Bhagoji2018}.

\section{Memorizacija modela i zaključivanje o članstvu}
Dok su prethodna dva napada problematična jer napadaju točnost protokola, postoji i napad koji izravno narušava privatnost podataka na kojima je neki model treniran. U njemu se izravno napada trenirani model koji se promatra kao crna kutija. Pretpostavlja se da postoji neka javna funkcija koja za neki primjer i trenirani model vraća predikciju tog primjera. Pritom sam model, struktura ili težine modela uopće ne moraju biti dostupne napadaču \cite{Melis2018, Truex2018}.

Budući da napadač zna kakve primjere model prima, može pretpostaviti kako izgledaju podaci na kojima je model treniran. Prvo generira nasumični skup podataka kojem je cilj biti što sličniji originalnom. Zatim sam trenira mnogo različitih modela i pokušava naći one koji se ponašaju "najbliže" modelu koji napada. Kada je pronađen model koji najbolje opisuje to svojstvo, na njemu će se pokretati predikcija za primjere koji su bili prisutni u treniranju tog modela, i za svaki primjer će se
pamtiti njegova predikcija (npr. u binarnoj klasifikaciji, pohranit će se primjer i vjerojatnosti pripadanja u prvoj ili drugoj klasi). Ti podaci spremat će se u jednak skup. Nakon toga, generira se drugi skup podataka koji simulira podatke koji nisu bili prisutni tijekom treniranja modela koji se napada. Za njih se također pamte njihove predikcije i odvaja ih se u drugi skup na isti način.
Ta dva skupa se pak koriste za treniranje novog modela, koji će biti ključan za napad. Naime, taj model naučit će razlikovati između primjera koji su bili prisutni u treniranju nekog modela i onih koji nisu, i tim modelom se onda napada onaj prvi model \cite{Melis2018, Truex2018}.

    Očito, ovaj napad ne napada protokol nego same modele, tako da unatoč pokazanim svojstvima postoji mogućnost narušavanja privatnosti podataka korisnika. Štoviše, ako su sudionici protokola maliciozni, pokazuje se da mogu koristiti vlastite podatke i poznavanje modela kako bi efikasnije napali model i narušili privatnost podataka ostalih korisnika \cite{Melis2018, Truex2018}.
