\label{sec:opis_protokola}

Protokol se odvija u mreži, čiji su dio server koji želi agregirati neke podatke, i \(n\) klijenata čije podatke server želi agregirati. Konkretno, svaki klijent (dalje označen s \(u\)) ima vlastiti vektor vrijednosti \(\mathbf{x}_u\), a cilj servera je izračunati \(\sum_u{\mathbf{x}_u}\). Radi jednostavnosti, pretpostavlja se da su elementi vektora \(x_u\) cijeli brojevi unutar nekog konačnog polja, ali to ograničenje se u implementaciji lagano proširi i na realne brojeve. Time se osigurava korektnost kada
je potrebno računati realne gradijente. 

Protokol se odvija u iteracijama, a svaka iteracija počinje tako da svaki klijent \( u\) iz nasumično odabranog podskupa svih klijenata otvara sigurni komunikacijski kanal (npr. uporabom TLS-a) prema serveru. Jedna iteracija se sastoji od pet faza. Svaku fazu osim prve započinje server tako da šalje određene podatke svim klijentima. Oni ih obrađuju
i potom šalju serveru. Time server, osim što agregira podatke, također djeluje kao voditelj protokola jer on započinje svaku fazu osim prve.

\section{Cilj protokola}
\label{sec:cilj_protokola}

Glavni cilj je omogućiti sumiranje svih vektora \(\mathbf{x}_u\). Pritom server ne smije moći saznati bilo koju pojedinu vrijednost \(\mathbf{x_u}\). Također, ni korisnik \(u\) ne smije moći saznati vrijednost \(\mathbf{x}_v\) bilo kojeg drugog korisnika \(v \ne u\). U tom smislu kaže se da su vektori \(\mathbf{x}_u\) \textit{sigurni}, tj. da je postupak sumiranja \textit{siguran}.

Kako bi se to ostvarilo, prave vrijednosti će se maskirati nasumično generiranim aditivnim šumom. Pritom je način maskiranja konstruiran tako da ukupna suma svih maskiranih vrijednosti svih korisnika ima jednak iznos kao i suma svih početnih vrijednosti bez šuma. Cijeli postupak je opisan u potpoglavlju \ref{sec:maskiranje}.

Drugi cilj protokola je omogućiti da radi u okruženju u kojemu lagano mogu prekinuti svoju komunikaciju sa serverom. Taj zahtjev dolazi iz stvarnih situacija gdje je ispadanje korisnika iz mreže izgledno. Na primjer, ako se protokol koristi za izračun gradijenta nekog modela koji se koristi u pametnim tipkovnicama na pametnim mobitelima, vjerojatno je da će neki korisnici usred izračuna isključiti uređaje kako bi smanjili potrošnju baterije ili će pak isključiti mobilnu mrežu
radi smanjenja mobilnog prometa. Radi toga, u protokolu je bitno pratiti koliko korisnika je preostalo i koliko ih je obustavilo komunikaciju. To svojstvo naziva se svojstvom \textit{otpornosti na ispadanje}. Informacija o ispalim i preostalim klijentima bit će ključna u otklanjanju aditivnog šuma, što je također opisano u potpoglavlju \ref{sec:maskiranje}.

\section{Maskiranje vrijednosti podataka}
\label{sec:maskiranje}
Radi jednostavnosti, pretpostavimo da su podaci korisnika \(u\) elementi nekog konačnog polja \(\mathbb{Z_R}\), gdje je \(R\) neki prosti broj. Tada ih možemo zapisati kao vektor \(\mathbf{x_u}\). Neka je \(\mathcal{U}\) skup svih korisnika t.d. korisnici zadovoljavaju relaciju potpunog poretka (npr. svakom korisniku se pridružuje jedinstveni redni broj). Tada je moguće generirati šum sljedećim izrazom\cite{Bonawitz2017}:

\begin{equation}
\label{eq:noise_gen}
    \mathbf{y_{u}} = \mathbf{x_{u}} + \mathbf{PRG(b_{u})} + \sum_{v \in \mathcal{U}:u<v}{\mathbf{PRG(s_{u,v})}} - \sum_{v \in \mathcal{U}:u>v}{\mathbf{PRG(s_{v,u})}} \pmod R
\end{equation}

Ovdje je \(PRG\) generator pseudo-nasumičnih brojeva koji kao parametar prima neki \textit{seed}. Za \textit{seed} \(\mathbf{b_{u}}\), ta funkcija predstavlja \textbf{osobni} šum korisnika \(u\). Treći član predstavlja aditivni šum svih korsinika \(v\) čiji indeks je veći od indeksa korisnika \(u\), a četvrti član predstavlja šum onih ostalih korisnika čiji indeks je manji od korisnika \(u\). 

\section{Korišteni kriptografski primitivi}
\label{sec:kriptografski_primitivi}

Jedan od zahtjeva protokola je da komunikacijski kanali budu "sigurni". U slučaju ovog protokola, to podrazumijeva povjerljivost i autentičnost. Također, bitno je omogućiti sigurnu razmjenu \textit{seedova} između parova klijenata. Za ostvarivanje tih zahtjeva potrebni su nam kriptografski primitivi. To su kriptografski algoritmi niske razine pomoću kojih se grade kriptografski protokoli. U izgradnji ovog protokola koriste se razmjena ključeva, autentificirana enkripcija,
Shamirova shema za dijeljenje tajni, kriptografski sigurni generatori pseudo-nasumičnih brojeva i digitalni potpisi. Svaki od njih je detaljnije opisan u nastavku, zajedno sa svojom ulogom u protokolu.

\subsection{Autentificirana enkripcija}

Povjerljivost podrazumijeva da neka informacija ne smije biti dostupna pojedincima kojima nije namijenjena. Autentičnost garantira da je informacija "iskrena", tj. originalna ili nepromijenjena. To znači da je moguće provjeriti valjanost nepromijenjenog prijenosa te informacije, same informacije ili njezinog izvora.

Autentificirana enkripcija (engl. \textit{authenticated encryption}, dalje AE) je oblik enkripcije koji kombinira ta navedena dva svojstva.
Neka je \(p\) poruka koju korisnik želi kriptirati (engl. \textit{plaintext}), \(c\) kriptirana poruka (engl. \textit{ciphertext}), \(k\) ključ kojom se \(p\) kriptira (engl. \textit{encryption key}) i \(t\) oznaka autentičnosti (engl. \textit{authentication tag} ili \textit{message authentication code (MAC)}). Tada AE ima sljedeća dva svojstva:

\begin{equation}
\label{eq:ae_encryption}
    E(p, k) = c || t
\end{equation}

\begin{equation}
\label{eq:ae_decryption}
    D(c || t, k) = p
\end{equation}

Funkcije \(E\) i \(D\) označavaju funkcije enkripcije i dekripcije, a \(||\) je operator konkatenacije. Oznaka autentičnosti garantira da sadržaj nitko nije mijenjao.
U pravilu, ključ \(k\) će biti isti za obje strane komunikacije, što se onda naziva \textit{simetričnom enkripcijom}.
Konačno, u protokolu se AE koristi kako bi se sve poruke između servera i pojedinog klijenta kriptirale.

\subsection{Razmjena ključeva}
Ako se pretpostavlja da je komunikacijski kanal nesiguran i da se sigurnost dobiva enkripcijom koja zahtijeva neke ključeve, također je potreban neki način kojim bi se ti ključevi sigurno razmijenili u tom nesigurnom kanalu.

Protokol razmjene ključeva (engl. \textit{key-exchange protocol, key-agreement protokol}) je protokol kojim se dvije strane dogovaraju koji ključ će koristiti tijekom enkripcije. Ako je on korektno implementiran, također će spriječiti da nepouzdane treće strane bilo kako saznaju ili utječu na dogovorene ključeve. Rezultat protokola je zajednički ključ koji se dalje može koristiti za simetričnu enkripciju.
Protokol \textit{Diffie-Hellman} (dalje DH) je jedan od prvih takvih protokola, i koristi se kao primitiv u mnogim složenijim protokolima.

Sigurnost DH-a dolazi iz složenosti izračuna diskretnih logaritama.
Neka je \(p\) prost broj. Onda je primitivni korijen broja \(p\) onaj broj čije potencije modulo \(p\) generiraju sve cijele brojeve od \(1\) do \(p - 1\). Drugim riječima, ako je \(a\) primitivni korijen broja \(p\), onda se dobije neka permutacija skupa \(\{1, 2, \ldots, p - 1\}\) u obliku
\begin{equation*}
    a \text{ mod } p, a^2 \text{ mod } p, \ldots, a^{p - 1} \text{ mod } p 
\end{equation*}
Za bilo koji cijeli broj \(b\) i primitivan korijen \(a\) prostog broja \(p\), moguće je pronaći jedinstveni eksponent \(i\) t.d. vrijedi
\begin{equation*}
    b \equiv a^i \pmod{p}, \text{ gdje } 0 \leq i \leq (p - 1)
\end{equation*}
Taj eksponent \(i\) zove se \textit{diskretni logaritam} broja \(b\) za bazu \(a\), modulo \(p\) i označava se kao \(\text{dlog}_{a, p}(b)\) \cite{Stallings:2013:CNS:2523199}.

Sam DH temelji se na izračunu potencija u relativno velikim konačnim poljima, i za njegovu funkcionalnost potrebna su dva dogovorena javna parametra: prosti broj \(q\) i cijeli broj \(\alpha\) koji je primitivni korijen broja \(q\).

Pretpostavimo da korisnici A i B žele uspostaviti zajednički ključ. Korisnik A bira nasumični cijeli broj \(X_A < q\) i računa \(Y_A = \alpha^{X_A} \text{ mod } q\). Analogno tome, korisnik B isto tako bira nasumični cijeli broj \(X_B < q\) i računa \(Y_B = \alpha^{X_B} \text{ mod } q\). Oba korisnika čuvaju svoje \(X\)
vrijednosti privatnima, a onom drugom korisniku šalju vrijednost \(Y\) koja tada postaje javna. Time se dobije par javnog i odgovarajućeg privatnog ključa, \((X_A, Y_A)\) za korisnika A i \((X_B, Y_B)\) za korisnika B. Konačno, kako bi odredili zajednički ključ, korisnici potenciraju javni ključ "sugovornika" svojim privatnim ključem. Dakle, zajednički ključ \(K\) dobije se kao \(K = (Y_B)^{X_A} \text{ mod } q = (Y_A)^{X_B} \text{ mod } q\), i on se kasnije može koristiti za
enkripciju nekim simetričnim kriptosustavom.

Kako bi se pokazala sigurnost protokola, uzima se u obzir suparnik koji promatra tok komunikacije. Njemu su poznate vrijednosti \(q, \alpha, Y_A, Y_B\), ali ne zna niti \(X_A\) niti \(X_B\). Ako želi otkriti, na primjer, privatni ključ korisnika B, onda mora izračunati
\begin{equation*}
    X_B = \text{dlog}_{a, q}(Y_B)
\end{equation*}
i tek nakon toga dobiti \(K\). Sigurnost dolazi iz činjenice da je, u određenim uvjetima, izračun diskretnog logaritma izrazito računalno složen \cite{Stallings:2013:CNS:2523199}.

U zadnje vrijeme, radi efikasnosti i sigurnosti neka matematička svojstva protokola se mijenjaju. Na primjer, ne koriste se konačna polja nego grupe i operator množenja mijenja se operatorom zbrajanja. To je dovelo do razvoja varijante DH-a koja se naziva \textit{Diffie-Hellmanom nad eliptičnim krivuljama} (engl. \textit{Elliptic-curve Diffie-Hellman}), i ta varijanta se koristi u implementaciji protokola.

\subsection{Shamirova shema za dijeljenje tajni}

Ostaje pitanje kako će korisnik \(u\) serveru dati \textit{seed} \(\mathbf{b_{u}}\) koji je serveru potreban za uklanjanje osobnog šuma, a da pritom server ne može u bilo kojem trenutku izračunati samo privatne vrijednosti tog korisnika. Zato klijent ne šalje cijeli \textit{seed} serveru. Umjesto toga, on koristi Shamirovu shemu za podjelu tajni (engl. \textit{Shamir's Secret Sharing}, dalje SSS).

        Neka korisnik \(u\) ima tajnu \(S\) koja je element nekog konačnog polja \(\mathbb{F}\) reda \(R\). Onda je moguće podijeliti \(S\) na \(k\) dijelova, t.d. je potrebno barem \(t\) dijelova (\(t \le k\)) da bi se \(S\) ponovno mogao izračunati. Također, ako bilo tko ima samo \(t - 1\) dijelova, on neće moći izračunati \(S\) \cite{Shamir1979}.

Ova shema temelji se na činjenici da se iz \(n\) točaka može izračunati polinom (\(t - 1\))-og stupnja. Polinom se zadaje sljedećim izrazom (\ref{eq:poly}):
\begin{align}
\label{eq:poly}
    f(x)& = a_0 + a_1x + a_2x^2 + \ldots + a_{t - 1}x^{t - 1}& \pmod R
\end{align}
    gdje je koeficijent \(a_0 = S\), a koeficijenti \(a_1 \ldots a_{n - 1}\) nasumično izabrani elementi polja \(\mathbb{F}\). Korisnik \(u\) potom nasumično generira onoliko vrijednosti \(x\) koliko ima preostalih korisnika \(v\). Za svaki \(x_i\) računa vrijednost polinoma \(f(x_i)\) i dobiva \(S_i = (x_i, f(x_i))\). Svaki \(S_i\) predstavlja jedan dio tajne \(S\), i potrebno ih je minimalno \(t\) da bi se \(S\) rekonstruirao \cite{Shamir1979}.

Za rekonstrukciju se koristi Lagrangeova interpolacija polinoma definirana sljedećim izrazom (\ref{eq:lagrange}):
\begin{equation}
\label{eq:lagrange}
    L(x) := \sum_{j = 0}^{t - 1}f(x_j)l_j(x) \pmod R
\end{equation}
gdje \(l_j\) predstavlja Lagrangeov bazni polinom:
\begin{equation}
\begin{split}
    l_j(x)& = \prod_{0 \le m \le k, m \ne j}{\frac{x - x_m}{x_j - x_m}} \pmod R\\
    & = \frac{(x - x_0)}{(x_j - x_0)} \dots \frac{(x - x_{j-1})}{(x_j - x_{j-1})}
    \frac{(x - x_{j+1})}{(x_j - x_{j+1})} \dots \frac{(x - x_t)}{(x_j - x_t)}
    \pmod R
\end{split}
\end{equation}

Budući da je tajna \(S\) slobodni član polinoma, nije potrebno računati sve koeficijente nego je dovoljno izračunati \ref{eq:lagrange} u točki \(x = 0\). Time se dobije konačni izraz za rekonstrukciju tajne (\ref{eq:reconstruct}):
\begin{equation}
\label{eq:reconstruct}
    S = L(0) = \sum_{j = 0}^{t - 1}{f(x_j)\prod_{m = 0, m \ne j}^{t - 1}{\frac{x_m}{x_m - x_j}}} \pmod R
\end{equation}

    Bitno je napomenuti da se za rekonstrukciju ne moraju koristiti svi dijelovi tajne, nego samo \(t\) njih ako je broj dijelova veći od \(t\). Taj broj \(t\) zove se "prag sheme" (engl. \textit{threshold}). Također je bitno paziti na odabir reda \(R\) polja \(\mathbb{F}\). Nužno je da \(R>S\) jer se u suprotnom gubi sigurnost. Budući da se u protokolu dijele \textit{seedovi} i 256-bitni tajni ključevi, za parametar \(R\) se bira prvi prosti broj nakon \(2^{256}\).

\subsection{Primitivi u implementaciji}
\label{sec:primitivi_impl}
Svi navedeni primitivi korišteni su u samoj implementaciji u programskom jeziku \textit{Rust}. Razmjena ključeva i AE ostvareni su korištenjem paketa \textit{box\_} iz gotove biblioteke \textit{sodiumoxide}, pozivom funkcija \textit{seal} i \textit{unseal}, koje kombiniraju primitive Curve25519, Salsa20 i Poly1305. Za SSS nije korištena potojeća biblioteka nego je razvijena nova, prilikom koje je bilo potrebno koristiti \textit{OpenSSL}-ovu biblioteku za aritmetiku velikih
brojeva. Obje kriptografske biblioteke prethodno su inicijalizirane njihvom vlastitim \textit{init} funkcijama, bez kojih je njihovo korištenje nesigurno. Također je bilo potrebno koristiti sigurni generator nasumičnih brojeva koji je ponuđen u biblioteci \textit{rand}.

\section{Faze protokola}
\label{sec:faze_protokola}
U poglavlju \ref{sec:opis_protokola} je navedeno da se protokol dijeli na pet faza. Bitno je napomenuti da u bilo kojem trenutku svake od njih klijenti mogu napustiti sudjelovanje u protokolu i na početku svake faze osim prve provjerava se je li broj trenutnih klijenata pao ispod praga \(t\). Ako je, odvijanje protokola se zaustavlja. Komunikacija između klijenta i servera se zaustavlja ako je autentičnost neke njihove poruke narušena. Također, kada se navodi da server
šalje ili prima poruke od svih klijenata, podrazumijeva se da se sljedeći korak protokola izvodi ako je izmijenio poruke sa svim trenutno aktivnim korisnicima ili je istekao neki vremenski period, čime se stvara tolerancija na ispad klijenata iz same mreže.

\subsection{Faza inicijalizacije}
\label{ssec:init}
Prva faza protokola je faza inicijalizacije (orig., \textit{setup phase}\cite{Bonawitz2017}). U njoj se inicijaliziraju kriptografske biblioteke i uspostavljaju sigurne veze između klijenata i servera. Također, svi sudionici inicijaliziraju sljedeće parametre:
\begin{itemize}
    \item red \(k\) konačnog polja \(\mathbb{F}\) - uvijek se bira prvi prosti broj veći od \(2^{256}\)
    \item broj korisnika koji sudjeluju u protokolu \(n\)
    \item prag SSS-a \(t\)
\end{itemize}
koji su svi nužni za funkcionalnost SSS-a.

\begin{figure}
    \centering
    \includegraphics[scale=0.65]{diag/inicijalizacija.png}
    \caption{Dijagram faze inicijalizacije}
    \label{fig:init}
\end{figure}

\subsection{Faza objavljivanja ključeva}
\label{ssec:advertise}
U fazi objavljivanja ključeva (orig., \textit{AdvertiseKeys}\cite{Bonawitz2017}) svaki klijent \(u\) generira dva para javnih i privatnih ključeva, \((c_{u}^{PK}, c_{u}^{SK})\) i \((s_{u}^{PK}, s_{u}^{SK})\). U prvom paru, \(c\) označava da se radi o komunikaciji (engl. \textit{communication}), i taj par se koristi kako bi klijenti međusobno mogli komunicirati sigurno preko posrednika servera. Drugi par se koristi u generaciji zajedničkih \textit{seedova} potrebnih za izračun šuma.


Dalje, svaki klijent serveru šalje uređeni par javnih ključeva \((c_{u}^{PK}, s_{u}^{PK})\). Kada server primi parove od svih korisnika, on ih indeksira i šalje svim korisnicima (osim para koji odgovara korisniku kojemu se poruka šalje). U samoj implementaciji, granica između kraja prve i početka druge faze je mutna, jer se uređeni par javnih ključeva šalje serveru odmah nakon otvaranja sigurnih kanala.

\begin{figure}
        \centering
        \includegraphics[scale=0.65]{diag/objavljivanje-alt.png}
        \caption{Dijagram faze objavljivanja ključeva}
        \label{fig:advertise}
\end{figure}

\subsection{Faza podjele ključeva}
\label{ssec:sharekeys}
Faza podjele ključeva (orig., \textit{ShareKeys}\cite{Bonawitz2017}) počinje tako da svaki klijent \(u\) primi indeksirani popis parova \((v, c_{v}^{PK}, s_{v}^{PK})\) iz prethodne faze. Iz te poruke klijent može zaključiti na koliko dijelova će trebati podijeliti svoj tajni ključ \(s_u^{SK}\) i \textit{seed} \(b_u\), a prag \(t\) je javni parametar ustanovljen u fazi inicijalizacije (\ref{ssec:init}). Nakon toga, za sve klijente \(v\) se računaju njihovi dijelovi ključa i
\textit{seeda} (\(s_{u,v}^{SK}, b_{u,v}\)), koji se potom indeksiraju i kriptiraju kako bi se za svakog klijenta \(v\) dobila pripadajuća poruka \(e_{u,v} = u || v || s_{u,v}^{SK} || b_{u,v}\). Te poruke, zajedno s pripadajućim indeksima \(u\) i \(v\) klijent šalje serveru. Server prikuplja sve poruke, i na kraju faze svakom klijentu šalje one poruke koje u indeksu primatelja sadrže indeks tog klijenta.

Drugim riječima, u fazi podjele ključeva svaki klijent svoj tajni ključ \(s_u^{SK}\) i \textit{seed} \(b_u\) dijeli na onoliko dijelova koliko ima preostalih klijenata. Te dijelove kriptira tako da ih sakrije od servera i potom ih indeksira i šalje serveru koji ih samo prosljeđuje svim ostalim klijentima.

\begin{figure}
        \centering
        \includegraphics[scale=0.65]{diag/sharekeys.png}
        \caption{Dijagram faze podjele ključeva}
        \label{fig:sharekeys}
\end{figure}

\subsection{Faza prikupljanja maskiranih vrijednosti}
\label{ssec:maskedinput}
U fazi prikupljanja maskiranih vrijednosti (orig., \textit{MaskedInputCollection}\cite{Bonawitz2017}) svaki klijent svoje vrijednosti maskira svojim šumom i šumom svih ostalih klijenata od kojih je primio kriptiranu poruku u prethodnoj fazi. Potom te maskirane vrijednosti šalje serveru, koji ih rekonstruira u sljedećoj fazi. Na kraju faze, server svim klijentima šalje popis svih klijenata od kojih je primio maskirane vrijednosti.

Prilikom maskiranja, klijent \(u\) koristi \textit{seedove} \(b_u\) i \(s_{u,v}\), gdje se \(s_{u,v}\) računa posebno za svakog drugog klijenta \(v\). Dalje se vrijednostima \(\mathbf{x_u}\) pribrajaju \(\mathbf{p_u} = \mathbf{PRG(b_u)}\) i \(\mathbf{p_{u,v}} = \mathbf{PRG(s_{u,v})}\), gdje je \(p_{u,v}\) pozitivan ako \(u > v\) ili negativan ako \(u < v\). Ovaj postupak je ekvivalentan izrazu \ref{eq:noise_gen}.

\begin{figure}
        \centering
        \includegraphics[scale=0.65]{diag/maskedcollection.png}
        \caption{Dijagram faze prikupljanja maskiranih vrijednosti}
        \label{fig:collection}
\end{figure}

\subsection{Faza rekonstrukcije}
\label{ssec:unmasking}
Faza rekonstrukcije (orig., \textit{Unmasking}\cite{Bonawitz2017}) je posljednja faza protokola. Na početku faze, klijenti primaju popis svih ostalih klijenata koji su serveru poslali svoje maskirane vrijednosti. Dalje,  svaki korisnik dekriptira poruke koje je dobio od ostalih korisnika u fazi \ref{ssec:sharekeys}. Nakon toga serveru šalju dijelove \textit{seedova} pomoću kojih server rekonstruira \textit{seedove}. Rekonstruira se \(s_{u,v}\) za sve korisnike koji nisu poslali svoj vektor maskiranih vrijednosti, i \(b_u\) za sve ostale klijente. Konačno, računa se zbroj vrijednosti
\(\mathbf{z} = \sum_u{\mathbf{x_u}}\) koristeći izraz:

\begin{equation}
    \mathbf{z} = \sum_{u \in O}{\mathbf{y_u}} - \sum_{u \in O}{\mathbf{p_u}} + \sum_{u \in O, v \in I}{\mathbf{p_{v,u}}}
\end{equation}
gdje je \(O\) skup korisnika koji su ostali do faze rekonstrukcije, a \(I\) skup onih koji su u međuvremenu ispali, i \(\mathbf{p_u} = \mathbf{PRG(b_u)}\), \(\mathbf{p_{v,u}} = \mathbf{PRG(s_{v,u})}\) .

Popis preostalih klijenata ključan je u odlučivanju kojeg dijela tajne poslati. Iz njega i iz popisa koji su dobili u fazi podjele ključeva (\ref{ssec:sharekeys}), korisni mogu zaključiti koji klijenti su u međuvremenu ispali iz protokola. Tu informaciju koriste kako bi znali koji udio tajne treba poslati serveru. Ako je klijent \(v\) u međuvremenu ispao, klijent \(u\) šalje svoj dio tajnog ključa korisnika \(v\), \(s_{v,u}^{SK}\). Međutim, ako je klijent \(v\) bio
prisutan u obje faze protokola, onda se šalje dio njegovog osobnog \textit{seeda} \(b_{v,u}\).

Ova distinkcija je potrebna kako bi se sačuvala privatnost vektora \(\mathbf{x_u}\). Ako bi klijent \(u\) poslao dijelove obiju tajni, server bi onda imao mogućnost rekonstruirati pojedine vektore vrijednosti \(\mathbf{x_u}\) za svakog korisnika \(u\), čime bi se narušila privatnost.

\begin{figure}
        \centering
        \includegraphics[scale=0.65]{diag/unmasking.png}
        \caption{Dijagram faze rekonstrukcije}
        \label{fig:unmasking}
\end{figure}
