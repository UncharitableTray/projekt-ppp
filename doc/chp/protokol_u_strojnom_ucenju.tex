\label{sec:strojno}
Strojno učenje je proces rješavanja praktičnog problema tako da se prikuplja skup podataka, i na temelju tog skupa se algoritamski gradi statistički model koji rješava taj problem. Učenje može biti nadzirano, polu-nadzirano, nenadzirano ili podržano\cite{hundredpageml}. U nastavku je opisano nadzirano učenje, budući da je ono najčešći oblik strojnog učenja.

\section{Nadzirano učenje}
\label{sec:supervised}
U nadziranom učenju, skup podataka je kolekcija \textit{označenih primjera} \({(\mathbf{x}_i, y_i})_{i=1}^{N}\). Svaki element \(\mathbf{x}_i\), kojih ima \(N\), naziva se \textit{vektor značajki}. To je vektor čija svaka dimenzija \(j = 1, \ldots, D\) sadrži vrijednost \(x^(j)\) koja na neki način opisuje pripadajući primjer. Vrijednost \(y_i\) zove se \textit{oznaka} i može biti ili element konačnog skupa \textit{razreda} \({1, 2, \ldots, C}\), realni broj, ili
rjeđe, neka složenija struktura poput vektora ili matrice\cite{hundredpageml}.

Cilj algoritma nadziranog učenja je iskoristiti skup podataka kako bi se stvorio model koji kao ulaz prima vektor značajki \(\textbf{x}\) i vraća neku informaciju pomoću koje je onda moguće odrediti oznaku za taj vektor značajki\cite{hundredpageml}. Ako primjeru pridružujemo razred, onda se radi o \textit{klasifikacijskom problemu}, a u \textit{regresijskom problemu} primjeru se pridružuje neka kontinuirana vrijednost\cite{SnajderStrojno}.

\subsection{Linearna regresija}
\label{ssec:linreg}
Na primjer, u linearnoj regresiji, vektor značajki i oznaka primaju vrijednosti iz domene realnih brojeva, i model poprima oblik linearne funkcije:
\begin{equation}
    f_{\mathbf{w},b}(\mathbf{x}) = \mathbf{wx} + b
\end{equation}
gdje je \(\mathbf{w}\) neki \(D\)-dimenzionalni vektor težina i \(b\) je realni broj.
Cilj algoritma strojnog učenja je pronaći optimalne vrijednosti \((\mathbf{w}^*, b^*)\) takve da za bilo koji \(\mathbf{x}\), \(y = f_{\mathbf{w}^*, b^*}(\mathbf{x})\) bude najprecizniji, tj. najtočnije opisuje situaciju u stvarnom svijetu koju statistički model pokušava predvidjeti \cite{hundredpageml}.

Točnost modela može se mjeriti na više načina i ovisi o konkretnom problemu koji se rješava. Za linearnu regresiju, mjera točnosti, tj. pogreške modela opisuje se kvadratnom pogreškom:
\begin{equation}
    L(\mathbf{x}) = \frac{1}{N}\sum_{i=1 \ldots N}{(f_{\mathbf{w}^*, b^*}(\mathbf{x}) - y_i)^2}.
\end{equation}
Budući da se ovim izrazom mjeri iznos pogreške, manja vrijednost ukazivat će na manju pogrešku, tj. veću točnost. Dakle, cilj je \textit{minimizirati} kvadratnu pogrešku. Postoji više strategija kojima se te takve funkcije optimiraju, ali radi jednostavnosti se samo opisuje jedan, najčešći postupak koji se zove \textit{gradijentni spust} (engl. \textit{gradient descent}).

\subsection{Gradijentni spust}
\label{ssec:graddescent}
Gradijentni spust je iterativni optimizacijski algoritam kojim se traži minimum neke funkcije koja mora biti diferencijabilna. Zatim se računa gradijent pogreške za neki zadani primjer, i vrijednost trenutne točke (u strojnom učenju to je vektor \(\mathbf{w}\) i parametar \(b\)) pomiče se u smjeru suprotnom od smjera gradijenta skalirana nekom vrijednosti \(\eta \in (0, 1)\) koja se naziva \textit{stopom učenja}. U pravilu, parametar \(b\) je uključen u vektor težina \(\mathbf{w}\) kao njegova nulta ili \(D\)-ta dimenzija.

Prije početka, potrebno je izračunati gradijent funkcije gubitka. Derivira se po vektoru \(\mathbf{w}\) jer je to vektor težina koji želimo optimirati:

\begin{equation}
    \nabla L = \Big(\frac{\partial L}{\partial w^{(1)}}, \frac{\partial L}{\partial w^{(2)}}, \ldots, \frac{\partial L}{\partial w^{(D)}}\Big)
\end{equation}

Na primjer, u slučaju dvodimenzionalnog vektora \(\mathbf{w}\) u linearnoj regresiji, gradijent pogreške zadan je sljedećim izrazom:

\begin{equation}
    \nabla L = \Big(\frac{\partial L}{\partial w}, \frac{\partial L}{\partial b}\Big)
\end{equation}
uz parcijalne derivacije
\begin{align*}
    \frac{\partial L}{\partial w}& = \frac{1}{N}\sum_{i=1}^{N}-2x_i(y_i - (wx_i + b))\\
    \frac{\partial L}{\partial b}& = \frac{1}{N}\sum_{i=0}^{N}-2(y_i - (wx_i + b))
\end{align*}

Na početku gradijentnog spusta, bira se neki početni vektor \(\mathbf{w}_0\). Zatim, u svakoj iteraciji \(n\) računa se novi vektor težina, sve dok pogreška ne postane dovoljno malena za neki veliki skup primjera ili dok ne prođe određeni broj iteracija, sljedećim izrazom:

\begin{equation}
    \mathbf{w}_n \leftarrow \mathbf{w}_{n - 1} - \eta \nabla L
\end{equation}

\subsection{Logistička regresija}
\label{ssec:logreg}
Iako se zove regresijom, logistička regresija je zapravo klasifikacijski algoritam. Naime, zove se regresijom jer ime dolazi iz statistike i zadržalo se jer je matematički jako slična linearnoj regresiji. Radi se o jednostavnijem linearnom modelu. Opisana na primjeru \textit{binarne} klasifikacije, gdje se primjeru pridružuje jedan od dva moguća razreda (za razliku od \textit{višerazredne/multinomne klasifikacije}) \cite{hundredpageml}.

Ovdje je još uvijek potrebno modelirati \(y_i\) kao linearnu funkciju \(f_{\mathbf{w}*, b*}(\mathbf{x})\). Međutim, u onakvom obliku kakav se koristi u linearnoj regresiji, vrijednost \(y_i\) može uzeti bilo koju realnu vrijednost. U logističkoj regresiji je potrebno nekim mehanizmom ostvariti pridjeljivanje jednoga od dvaju ili više razreda ulaznom vektoru značajki \(\mathbf{x}\).

U binarnom slučaju, najjednostavnije je komponirati funkciju \(f\) s drugom funkcijom koja realnu domenu preslikava u interval \((0, 1)\). Jedna takva funkcija je funkcija \textit{sigmoida} (također \textit{standardna logistička funkcija}):

\begin{equation*}
    \sigma (x) = \frac{1}{1 + e^{-x}}
\vspace{0.5cm}
\end{equation*}
pomoću koje se lako izvodi model logističke regresije:

\begin{equation}
\label{eq:logreg}
    f_{\mathbf{w}, b}(\mathbf{x}) = \frac{1}{1 + e^{- (\mathbf{wx} + b)}}.
\vspace{0.5cm}
\end{equation}

Kao u linearnoj regresiji, također je potrebno definirati neku mjeru pogreške. Ovdje kvadratna pogreška više nije dobra mjera jer se "udaljenost" ne računa izravno nego je samo potrebno znati je li ulazni vektor značajki \(\mathbf{x}\) unutar nekog razreda ili nije. Umjesto toga, definira se nova funkcija koja računa \textit{izglednost} primjera. Ovu funkciju više nije cilj minimizirati nego maksimizirati, i zato se zove funkcija \textit{maksimalne
izglednosti}:

\begin{equation}
\label{eq:maxlikelihood}
    L_{\mathbf{w}, b} = \prod_{i = 1, \ldots, N}f_{\mathbf{w}, b}(\mathbf{x}_i)^{y_i} (1 - f_{\mathbf{w}, b}(\mathbf{x}_i))^{1 - y_i}
\end{equation}
Ova funkcija, za razliku od kvadratne pogreške, nema zatvorenu formu i zato je gradijentni spust pogodan za njenu optimizaciju.
%\begin{tikzpicture}[xscale=13,yscale=3.8]
%\draw [<->] (0,0.8) -- (0,0) -- (0.5,0);
%\draw[red, ultra thick, domain=0:0.5] plot (\x, {0.025+\x+\x*\x});
%\end{tikzpicture}

\section{Protokol u strojnom učenju}
Proces strojnog učenja odvojen je u dvije faze, treniranje i evaluaciju. Prilikom treniranja, vektor težina \(\mathbf{w}\) optimira se nekom od tehnika poput gradijentnog spusta. Evaluacija provjerava preciznost modela nakon što je odabran optimalni \(\mathbf{w}\). Prilikom oba ova postupka, koriste se neki podaci koji predstavljaju vektore \(\mathbf{x}\) kojima se model trenira ili evaluira, koji se nalaze u pripadajućim skupovima za
treniranje ili evaluaciju.

Tradicionalno, ti podaci su se skupljali centralizirano i tek onda bi počelo treniranje modela. U zajedničkom učenju, situacija se mijenja tako da se podaci ne centraliziraju nego svaki čvor ili klijent čuva svoje podatke kod sebe i umjesto toga sa serverom dijeli samo svoje gradijente. Implementirani protokol nastao je u tu svrhu. Osim samog skupljanja gradijenata, on također nudi garanciju privatnosti (kao što je opisano u potpoglavljima \ref{ssec:semihonest} i
\ref{ssec:malicious}).

Neka je \(\mathcal{U}\) skup svih korisnika i \(\mathcal{D}_u\) privatni skup primjera korisnika \(u\) koje on koristi kako bi trenirao model. Onda se ukupni skup raspodijeljen po svim korisnicima može zapisati kao \(\mathcal{D} = \cup_{u \in \mathcal{U}}\mathcal{D}_u\).

Ukupni gradijent se računa kao težinski prosjek gradijenata svih korisnika:
\begin{equation}
\label{eq:sumgradient}
    \nabla L = \frac{1}{|\mathcal{D}|}\sum_{u \in \mathcal{U}}|\mathcal{D}_u|\nabla L_u
\end{equation}

U slučaju da korisnici imaju puno primjera, umjesto da se trenira na cijelom skupu, bira se nasumični podskup primjera \(\mathcal{D}_u^t \subseteq \mathcal{D}_u\) takav da \(\mathcal{D}^t = \cup_{u \in \mathcal{U}_u^t}\mathcal{D}_u^t\) i \(\mathcal{D}^t \subseteq \mathcal{D}\). Ova metoda se zove \textit{stohastični gradijentni spust grupa} (engl. \textit{batch stochastic gradient descent}).
Ovim se dobiva na efikasnosi i ponekad pomaže u izbjegavanju lokalnih optimuma.

Konačno, kada je dobiven novi gradijent, pomak vektora težina može se izračunati na sljedeći način:
\begin{equation}
\label{eq:update_batch}
    \mathbf{w}_{n} \leftarrow \mathbf{w}_{n-1} - \eta \frac{\sum_{u \in \mathcal{U}}{|\mathcal{D}_u^t| \nabla L_u}}{\sum_{u \in \mathcal{U}}{|\mathcal{D}_u^t|}}
\end{equation}
U implementaciji je najjednostavnije kao privatnu vrijednost \(\mathbf{x}_u\) klijenta \(u\) odabrati \(|\mathcal{D}_u^t|\nabla L\) i još konkatenirati veličinu grupe ili skupa za treniranje, ovisno o tome koristi li se SGD ili obični gradijentni spust.

\subsection{Eksperiment}
Protokol je testiran na primjeru logističke regresije. Deterministički su generirane točke unutar kružnica radijusa \(3\) oko točaka \((4, 10)\) i \((10, 4)\) (slika \ref{fig:a}). Koordinate simetrične oko osi \(y = x\) su odabrane radi lakše intuitivne validacije modela. Biblioteka za logističku regresiju je implementirana ručno. Točke koje leže oko prve kružnice (dakle, iznad osi \(x = y\) ) označene su oznakom \textit{One}, a one oko druge su označene kao \textit{Zero}.

\begin{figure}
\centering
    \subfigure[Skup podataka treniranja]{\label{fig:a}\includegraphics[width=60mm]{img/plot.png}}
    \subfigure[Dobivena decizijska granica]{\label{fig:b}\includegraphics[width=60mm]{img/plane.png}}

    \caption{Slika \ref{fig:a} prikazuje generirani skup podataka na kojemu se logistička regresija trenira. Slika \ref{fig:b} prikazuje dobivenu ravninu koja približno dijeli hiperravninu određenu \(xy\)-osima simetrično oko osi \(y = x\). Ta ravnina dobivena je nakon 100 iteracija i koristi se prilikom predikcije novih primjera.}
\end{figure}

Cilj eksperimenta je provjeriti valjanost modela nakon sigurne agregacije gradijenata postupkom koji je opisan u prethodnom poglavlju. Prvo se model trenira "lokalno", tj. provodi se samo postupak učenja na prethodno opisanom skupu podataka onako kako bi to činio samo jedan klijent. Potom se taj model uspoređuje s onim koji se dobije sigurnim agregiranjem, dakle, sigurnim zajedničkim učenjem. Taj drugi model dobije se tako da server na početku svake iteracije svim klijentima
šalje početni vektor težina, kojeg oni optimiraju jednim pokretanjem gradijentnog spusta. Server nakon toga računa težinski prosjek tih novih gradijenata pomoću protokola.

Postupak treniranja se u oba slučaja odvija u 100 iteracija. Tablica \ref{tab:gradients} prikazuje gradijente nakon skaliranja u prvoj, desetoj i stotoj iteraciji. Analogno tome, tablica \ref{tab:weights} prikazuje vektore težina u istim iteracijama.
Budući da se za lokalno učenje i zajedničko učenje kod svih klijenata koristio isti skup podataka i obični gradijentni spust (dakle, učilo se na svim primjerima, a ne podskupu istih), identični gradijenti i vektori težina su empirijski dokaz da protokol korektno računa sumu gradijenata. Kako su primjeri linearno odvojivi, testiranje oba modela daje preciznost od 1. Drugim riječima, model u oba slučaja uspješno klasificira 100\% primjera iz testnog skupa.

\begin{table}
    \centering
    \caption{Prikazan je slučaj kada 10 klijenata ima identične gradijente i jednaku veličinu skupa za testiranje. Iste vrijednosti uz istu stopu učenja pokazuju da algoritam radi korektno. Suma gradijenata dobivena je izrazom \ref{eq:sumgradient}.}
    \begin{tabular}{l | c | c}
        \label{tab:gradients}
        Iteracija & Lokalno učenje \((\nabla L)\) & Zajedničko učenje \((\sum \nabla L)\) \\
        \hline
        1 & (0.053087, -0.058995, -0.000420) & (0.053087, -0.058995, -0.000420) \\
        10 & (0.018817, -0.018999, -0.000047) & (0.018817, -0.018999, -0.000047) \\
        100 & (0.002475, -0.002544, -0.000011) & (0.002475, -0.002544, -0.000011) \\
    \end{tabular}
\end{table}

\begin{table}
    \centering
    \caption{Prikazani su vektori težina nakon promjene dobiveni pomoću izračunatih gradijenata prikazanih u tablici \ref{tab:gradients}.}
    \begin{tabular}{l | c | c}
        \label{tab:weights}
        Iteracija & Lokalno učenje \((\mathbf{w}_n)\) & Zajedničko učenje \((\mathbf{w}_n)\)\\
        \hline
        1 & (-0.144286, 0.147256, 0.0) & (-0.144286, 0.147256, 0.0) \\
        10 & (-0.481164, 0.478725, -0.000215) & (-0.481164, 0.478725, -0.000215) \\
        100 & (-0.972089, 0.980283, 0.001854) & (-0.972089, 0.980283, 0.001854) \\
    \end{tabular}
\end{table}
